import asyncio
import json
import traceback
import sys
import aiohttp
import discord
from discord.ext import commands, tasks

class Stream(commands.Cog):
    def __init__(self, client):
        self.client = client

    @commands.Cog.listener()
    async def on_ready(self):
        print('Stream Cog Loaded')
        self.livestream_test_loop.start()

    async def api_call_game_name(self, game_id):
        client_id = self.client.configp.get("TOKENS", "Twitch Token")
        twitch_api_url = f"https://api.twitch.tv/helix/games?id={game_id}"

        async with aiohttp.request("GET", twitch_api_url, headers={"Client-ID" : client_id, 'User-Agent' : "Magic Browser"}) as url:
            if url.status == 200:
                data = await url.json()
                return data['data'][0]['name']
            print('Not 200')
            return 0

    async def api_call_users(self, streamer_id):
        client_id = self.client.configp.get("TOKENS", "Twitch Token")
        twitch_api_url = f"https://api.twitch.tv/helix/users?id={streamer_id}"

        async with aiohttp.request("GET", twitch_api_url, headers={"Client-ID" : client_id, 'User-Agent' : "Magic Browser"}) as url:
            if url.status == 200:
                data = await url.json()
                return data
            print('Not 200')
            return 0

    async def api_call_streams(self, streamer_name):
        client_id = self.client.configp.get("TOKENS", "Twitch Token")
        twitch_api_url = f"https://api.twitch.tv/helix/streams?user_login={streamer_name}"

        async with aiohttp.request("GET", twitch_api_url, headers={"Client-ID" : client_id, 'User-Agent' : "Magic Browser"}) as url:
            if url.status == 200:
                data = await url.json()
                return data
            print('Not 200')
            return

    async def check_stream_status(self, data):
        if data['data'] != []:
            return True
        if data['data'] == []:
            return False

    async def send_notification(self, channel, streamer_name, streamer_url, streamer_pfp, stream_title, stream_game, custom_lmessage):
        embed = discord.Embed(
            title=custom_lmessage,
            colour=discord.Colour(0x832fc7),
            url=streamer_url,
            description=f"**Title:** {stream_title}\n**Game:** {stream_game}"
                )
        embed.set_thumbnail(url=streamer_pfp)
        embed.set_author(name=streamer_name, url=streamer_url)

        if streamer_name == 'minusgn':
            message = await channel.send(content='<@&431809823487557652>', embed=embed)
        elif streamer_name == 'aldusfrost':
            message = await channel.send(content='<@&632702906977746954>', embed=embed)
        else:
            message = await channel.send(embed=embed)

        with open('stream.json', 'r') as fn:
            data_json = json.load(fn)
        data_json['streamers'][streamer_name]['message'] = message.id

        with open('stream.json', 'w') as fn:
            json.dump(data_json, fn, indent=4, sort_keys=True)

    async def edit_notification(self, custom_nlmessage, streamer_name):
        embed = discord.Embed(title=custom_nlmessage, colour=discord.Colour(0x832fc7), url=f"https://www.twitch.tv/{streamer_name}")
        embed.set_author(name=streamer_name, url=f"https://www.twitch.tv/{streamer_name}")

        with open('stream.json', 'r') as fn:
            data_json = json.load(fn)

        message_id = data_json['streamers'][streamer_name]['message']
        channel_id = data_json['streamers'][streamer_name]['channel_id']
        channel = self.client.get_channel(channel_id)
        try:
            message = await channel.fetch_message(message_id)
            await message.edit(embed=embed)
        except: pass


    @tasks.loop(seconds=60)
    async def livestream_test_loop(self):
        try:
            with open('stream.json') as f:
                try:
                    self.num += 1
                except:
                    self.num = 0
                #print('loop ran: ' + str(self.num))

                streamers = json.load(f)
                for streamer_json in streamers['streamers'].items():

                    streamer_name = streamer_json[0]
                    data = await self.api_call_streams(streamer_name)
                    if data is None:
                        x = 1/0
                    #data = data.items()
                    #data = data[0]
                    status = await self.check_stream_status(data)

                    if status is False:
                        if streamer_json[1]['live_status'] == 1:
                            custom_nlmessage = streamer_json[1]['no_live_msg']
                            await self.edit_notification(custom_nlmessage, streamer_name)

                            with open('stream.json', 'r') as fn:
                                data_json = json.load(fn)
                            data_json['streamers'][streamer_name]['live_status'] = 0

                            with open('stream.json', 'w') as fn:
                                json.dump(data_json, fn, indent=4, sort_keys=True)

                    elif status is True:
                        user_id = data["data"][0]["user_id"]
                        data_user = await self.api_call_users(user_id)
                        #data_user = data_user.items()
                        #data_user = data_user[0]
                        if streamer_json[1]['live_status'] != 1:
                            channel_id = streamer_json[1]['channel_id']
                            channel = self.client.get_channel(channel_id)

                            streamer_url = f'https://www.twitch.tv/{data["data"][0]["user_name"]}'
                            streamer_pfp = data_user['data'][0]['profile_image_url']
                            custom_lmessage = streamer_json[1]['live_msg']

                            stream_title = data['data'][0]['title']
                            stream_game_id = data['data'][0]['game_id']
                            stream_game = await self.api_call_game_name(stream_game_id)

                            print(streamer_name)

                            if channel is not None:
                                await self.send_notification(channel, streamer_name, streamer_url, streamer_pfp, stream_title, stream_game, custom_lmessage)

                            with open('stream.json', 'r') as fn:
                                data_json = json.load(fn)
                            data_json['streamers'][streamer_name]['live_status'] = 1

                            with open('stream.json', 'w') as fn:
                                json.dump(data_json, fn, indent=4, sort_keys=True)
        except Exception as error:
            traceback.print_exception(type(error), error, error.__traceback__, file=sys.stderr)

    @commands.command(aliases=['cld', 'changedescription', 'clm', 'changelivemessage'])
    async def changelivedescription(self, ctx, *, message: str):
        with open('stream.json') as f:
            streamers = json.load(f)
            for streamer_json in streamers['streamers'].items():
                user = streamer_json[0]
                if streamers['streamers'][user]['discord_id'] == ctx.author.id:
                    with open('stream.json', 'r') as fn:
                        data_json = json.load(fn)
                    data_json['streamers'][user]['live_msg'] = message

                    with open('stream.json', 'w') as fn:
                        json.dump(data_json, fn, indent=4, sort_keys=True)

                    await ctx.send(f'Changed your live message to: `{message}`')
                    return
        await ctx.send('You do not seem to be binded with any of the streams currently being monitored.')



    @commands.command(aliases=['cnld', 'changenotlivemessage', 'cnlm'])
    async def changenotlivedescription(self, ctx, *, message: str):
        with open('stream.json') as f:
            streamers = json.load(f)
            for streamer_json in streamers['streamers'].items():
                user = streamer_json[0]
                if streamers['streamers'][user]['discord_id'] == ctx.author.id:
                    with open('stream.json', 'r') as fn:
                        data_json = json.load(fn)
                    data_json['streamers'][user]['live_msg'] = message

                    with open('stream.json', 'w') as fn:
                        json.dump(data_json, fn, indent=4, sort_keys=True)

                    await ctx.send(f'Changed your live message to: `{message}`')
                    return
        await ctx.send('You do not seem to be binded with any of the streams currently being monitored.')

    @commands.command(aliases = ['stream'])
    async def streamrole(self, ctx, clear=None):
        if ctx.guild.id == 159686161219059712:
            if clear is not None:
                role = ctx.guild.get_role(431809823487557652)
                roles = ctx.author.roles
                for position, item in enumerate(roles):
                    if item == role:
                        roles = roles.pop(position)
                await ctx.author.edit(roles)
                await ctx.send('Role removed')
            else:
                role = discord.utils.get(ctx.message.author.guild.roles, id=431809823487557652)
                await ctx.message.author.add_roles(role)
        elif ctx.guild.id == 367051646364024842:
            if clear is not None:
                role = ctx.guild.get_role(632702906977746954)
                roles = ctx.author.roles
                for position, item in enumerate(roles):
                    if item == role:
                        roles = roles.pop(position)
                await ctx.author.edit(roles)
                await ctx.send('Role removed')
            else:
                role = discord.utils.get(ctx.message.author.guild.roles, id=632702906977746954)
                await ctx.message.author.add_roles(role)



def setup(client):
    client.add_cog(Stream(client))
