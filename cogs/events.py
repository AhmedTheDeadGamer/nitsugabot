import discord
from discord.ext import commands

class OnEvents(commands.Cog):
    def __init__(self, client):
        self.client = client
        self.client.slof_count = {}
        self.emote_list = ['<:slof:422900553706766357>', '⬅️', '➡️', '🛑', '🔄']

    @commands.Cog.listener()
    async def on_ready(self):
        print('On Events Cog Loaded')

    @commands.Cog.listener()
    async def on_message(self, message):
        if message.author == self.client.user:
            return

        # uwu > owo (also omishi needs a live)
        #elif message.content.startswith('owo'):
        #    x = random.randint(1,1000)
        #    if message.author.id == 282622212207280139:
        #        await message.channel.send('<@282622212207280139>')
        #    elif x == 7:
        #        await message.channel.send('Notices Bulge <:eyesblur:502966639105409035>')
        #    else:
        #        await message.channel.send('uwu')

        #elif message.content.startswith('uwu'):
        #    if message.author.id == 282622212207280139:
        #        await message.channel.send('https://reddit.com/r/furry_irl <:kappa:501854269453107260>')

        #meep can kill me in 1 hit :sadfrog:
        if message.author.id == 380852450837528587:
        #if message.author.id == 182891574139682816:
            if '<@358985363496239105>' in message.content:
                exit()

        #wong slof dude
        if '<:slof:422900553706766357>' not in message.content:
            if 'GWchinaAstolfoSleep' in message.content:
                if message.guild.id == 159686161219059712:
                    await message.add_reaction("🤔")

        if message.guild is not None:
            if '<:slof:422900553706766357>' in message.content:
                if message.guild.id in self.client.slof_count:
                    self.client.slof_count[message.guild.id] += 1
                    if self.client.slof_count[message.guild.id] == 3:
                        await message.channel.send('<:slof:422900553706766357>')
                else:
                    self.client.slof_count[message.guild.id] = 1
            else:
                self.client.slof_count[message.guild.id] = 0

        #    await message.add_reaction('<:slof:422900553706766357>')
        if 'scp' in message.content.lower():
            await message.add_reaction('🚔')

        #amazing name tbh, For when the name was "The password is 123456"
        #elif message.content.startswith("******"):
        #    await message.channel.send("oh no, you hack me, sadface")
        #elif message.content == '123456':
        #    await message.channel.send('Not quite...')

        #fefer is a pussy
        elif "ping me on nsfw!" in message.content:
            await self.client.get_channel(354294536198946817).send(f"<@{message.author.id}>")
        elif "ping him on nsfw!" in message.content:
            await self.client.get_channel(354294536198946817).send("<@299624139633721345>")
        elif "discordapp.com/channels/" in message.content:
            msg = message.content.split('/')
            if len(msg) == 7:
                channel_id = msg[5]
                channel = self.client.get_channel(int(channel_id))
                if channel.is_nsfw():
                    await message.add_reaction("🇳")
                    await message.add_reaction("🇸")
                    await message.add_reaction("🇫")
                    await message.add_reaction("🇼")

        elif ':blank:563263278914994177' in message.content:
            await message.add_reaction('🤔')

        elif message.content.startswith('3.14'):
            pif = "3.1415926535897932384626433832795028841971693993751058209749445923078164062862089986280348253421170679821480865132823066470938446095505822317253594081284811174502841027019385211055596446229489549303819644288109756659334461284756482337867831652712019091456485669234603486104543266482133936072602491412737245870066063155881748815209209628292540917153643678925903600113305305488204665213841469519415116094330572703657595919530921861173819326117931051185480744623799627495673518857527248912279381830119491298336733624406566430860213949463952247371907021798609437027705392171762931767523846748184676694051320005681271452635608277857713427577896091736371787214684409012249534301465495853710507922796892589235420199561121290219608640344181598136297747713099605187072113499999983729780499510597317328160963185950244594553469083026425223082533446850352619311881710100031378387528865875332083814206171776691473035982534904287554687311595628638823537875937519577818577805321712268066130019278766111959092164201989"

            if len(pif) > len(message.content):
                l = len(message.content)
            else:
                l = len(pif)

            correct = True
            for i in range(l):
                if pif[i] != message.content[i]:
                    correct = False

            if correct:
                await message.add_reaction('✅')
            else:
                await message.add_reaction('❌')
        elif message.content.startswith('>rs') or message.content.startswith('>r'):
            await message.channel.send('Nice try.')

        #await self.client.process_commands(message)

    @commands.Cog.listener()
    async def on_message_edit(self, before, after):
        if before.content is not after.content:
            if before.author == self.client.user:
                return
            #else:
                #print(f'{before.author}: {before.content}\nEdited to: {after.content}\n ')

    @commands.Cog.listener()
    async def on_message_delete(self, message):
        if message.author == self.client.user:
            return
        #print(f'Deleted message:\n{message.author}: {message.content}\n ')

    @commands.Cog.listener()
    async def on_raw_reaction_add(self, payload):
        channel = self.client.get_channel(payload.channel_id)
        if channel is None: return
        message = await channel.fetch_message(payload.message_id)

        emoji = payload.emoji
        emoji_id = emoji.id

        if emoji_id is None:
            emoji = emoji.name
        else:
            maybe_emoji = self.client.get_emoji(emoji_id)
            if maybe_emoji is not None:
                emoji = maybe_emoji

        reaction = discord.utils.get(message.reactions, emoji=emoji)
        user = channel.guild.get_member(payload.user_id) if isinstance(channel, discord.TextChannel) else self.client.get_user(payload.user_id)

        self.client.dispatch("reaction_add_", reaction, user)

    @commands.Cog.listener()
    async def on_reaction_add(self, reaction, user):
        if reaction.message.author.bot:
            return
        if str(reaction) in self.emote_list: 
            if str(reaction) == self.emote_list[0]:
                return await reaction.message.add_reaction('<:slof:422900553706766357>')
            else: return
        elif str(reaction) == "🚫" or str(reaction) == "⛔" or str(reaction) == "🗑️":
            if reaction.message.author.id == self.client.user.id:
                return await reaction.message.delete()
        #else:
            #print(f'{reaction} added by: {user}\nOn message: {reaction.message.content} | in | {reaction.message.channel} | object | {reaction.message}\n ')
            #print(f'{reaction} added by: {user}')

    @commands.Cog.listener()
    async def on_reaction_remove(self, reaction, user):
        if reaction.message.author.bot:
            return
        if str(reaction) in self.emote_list: 
            if str(reaction) == self.emote_list[0]:
                if '<:slof:422900553706766357>' not in reaction.message.reactions:
                    await reaction.remove(self.client.user)
            else: return
        #print(f'{reaction} Removed by: {user}\nOn message: {reaction.message.content} | in | {reaction.message.channel} | object | {reaction.message}\n ')
        #print(f'{reaction} Removed by: {user}')


def setup(client):
    client.add_cog(OnEvents(client))
