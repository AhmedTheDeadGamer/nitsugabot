import discord
from discord.ext import commands
import asyncio
import youtube_dl
import re

youtube_dl.utils.bug_reports_message = lambda: ''


ytdl_format_options = {
    'format': 'bestaudio/best',
    'extractaudio': True,
    'audioformat': 'mp3',
    'outtmpl': '%(extractor)s-%(id)s-%(title)s.%(ext)s',
    'restrictfilenames': True,
    #'noplaylist': True,
    'nocheckcertificate': True,
    'ignoreerrors': False,
    'logtostderr': False,
    'quiet': True,
    'no_warnings': True,
    'default_search': 'auto',
    'source_address': '0.0.0.0' # bind to ipv4 since ipv6 addresses cause issues sometimes
}

ffmpeg_options = {
    'before_options': '-reconnect 1 -reconnect_streamed 1 -reconnect_delay_max 5',
    'options': '-vn',
}

ytdl = youtube_dl.YoutubeDL(ytdl_format_options)


class YTDLSource(discord.PCMVolumeTransformer): #taken from https://github.com/Rapptz/discord.py/blob/master/examples/basic_voice.py #IHaveNoIdeaHowThisWorksButIShould
    def __init__(self, source, *, data, volume=0.5):
        super().__init__(source, volume)

        self.data = data

        self.title = data.get('title')
        self.url = data.get('url')

    @classmethod
    async def from_url(cls, url, *, loop=None, stream=False):
        loop = loop or asyncio.get_event_loop()
        data = await loop.run_in_executor(None, lambda: ytdl.extract_info(url, download=not stream))

        if 'entries' in data:
            # take first item from a playlist
            data = data['entries'][0]

        filename = data['url'] if stream else ytdl.prepare_filename(data)
        return cls(discord.FFmpegPCMAudio(filename, **ffmpeg_options), data=data)

class Music(commands.Cog):
    def __init__(self, client):
        self.client = client

    @commands.Cog.listener()
    async def on_ready(self):
        print('Music Cog Loaded')


    @commands.command()
    async def specific_join(self, ctx, *, channel: discord.VoiceChannel):
        if ctx.voice_client is not None:
            return await ctx.voice_client.move_to(channel)
        await channel.connect()

    @commands.command()
    async def join(self, ctx):
        if ctx.voice_client is None:
            if ctx.author.voice:
                await ctx.author.voice.channel.connect()
            else:
                await ctx.send("You are not connected to a voice channel.")
                raise commands.CommandError("Author not connected to a voice channel.")


    @commands.command(aliases = ['lplay'])
    async def localplay(self, ctx, *, query):
        source = discord.PCMVolumeTransformer(discord.FFmpegPCMAudio(query))
        ctx.voice_client.play(source, after=lambda e: print('Player error: %s' % e) if e else None)
        await ctx.send('Now playing: {}'.format(query))

    @commands.command(aliases = ['dlplay', 'dplay'])
    async def downloadplay(self, ctx, *, url):
        if 'http' in url:
            if '<' not in ctx.message.content:
                try:
                    await ctx.message.delete()
                except:
                    pass
                await ctx.send('Put the url between <> so it doesn\'t embed the video.', delete_after=10)

            if '<' in ctx.message.content:
                url = re.sub('[<>]', '', url)

        async with ctx.typing():
            player = await YTDLSource.from_url(url, loop=self.client.loop)
            ctx.voice_client.play(player, after=lambda e: print('Player error: %s' % e) if e else None)
        await ctx.send('Now playing: {}'.format(player.title))

    @commands.command(aliases = ['splay', 'play'])
    async def streamplay(self, ctx, *, url):
        if 'http' in url:
            if '<' not in ctx.message.content:
                try:
                    await ctx.message.delete()
                except:
                    pass
                await ctx.send('Put the url between <> so it doesn\'t embed the video.', delete_after=10)

            if '<' in ctx.message.content:
                url = re.sub('[<>]', '', url)

        async with ctx.typing():
            player = await YTDLSource.from_url(url, loop=self.client.loop, stream=True)
            ctx.voice_client.play(player, after=lambda e: print('Player error: %s' % e) if e else None)
            #player = ctx.voice_client.play(player, after=lambda: check_queue(ctx.message.guild.id))

            #players[ctx.message.guild.id] = player
            #player.start()


        await ctx.send('Now playing: {}'.format(player.title))

    #@commands.command(aliases = ['que'])
    #async def queue(self, ctx, *, url):
    #    if '<' not in ctx.message.content:
    #        await ctx.send('Put the url between <> so it doesn\'t embed the video.')
    #        await ctx.message.delete()
    #    if '<' in ctx.message.content:
    #        url = re.sub('[<>]', '', url)

    #    async with ctx.typing():
    #        player = await YTDLSource.from_url(url, loop=self.client.loop, stream=True)
    #        #ctx.voice_client.play(player, after=lambda e: print('Player error: %s' % e) if e else None)

    #        player = ctx.voice_client.play(player, after=lambda: check_queue(ctx.message.guild.id))

    #        if server.id in queues:
    #            queues[server.id].append(player)
    #        else:
    #            queues[server.id] = [player]
    #    await ctx.send('Now playing: {}'.format(player.title))

    @commands.command()
    async def volume(self, ctx, volume: int):
        if ctx.voice_client is None:
            return await ctx.send("Not connected to a voice channel.")

        ctx.voice_client.source.volume = volume / 100
        await ctx.send("Changed volume to {}%".format(volume))

    @commands.command()
    async def stop(self, ctx):
        await ctx.voice_client.disconnect()

    @downloadplay.before_invoke
    @localplay.before_invoke
    @streamplay.before_invoke
    async def ensure_voice(self, ctx):
        if ctx.voice_client is None:
            if ctx.author.voice:
                await ctx.author.voice.channel.connect()
            else:
                await ctx.send("You are not connected to a voice channel.")
                raise commands.CommandError("Author not connected to a voice channel.")
        elif ctx.voice_client.is_playing():
            ctx.voice_client.stop()


    #@commands.command()

def setup(client):
    client.add_cog(Music(client))

