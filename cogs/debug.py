import subprocess
import difflib
import dukpy
import json
import traceback
import sys
import textwrap
import inspect
import os
from discord.ext import commands
import discord


class Debug(commands.Cog):
    def __init__(self, client):
        self.client = client
        self.allowed_list = [182891574139682816, 183671231944327179]

    async def ctxless_eval(self, code):
        env = {
            'client': self.client
        }

        env.update(globals())
        new_forced_async_code = f'async def code():\n{textwrap.indent(code, "    ")}' # https://stackoverflow.com/a/20158442/10630328 this is so i can use await on the d.py functions
        exec(new_forced_async_code, env) # compiles and runs the code, adds the returned object to env
        code = env['code'] # transforms the object into a function that is useable

        try:
            await code() # runs the function
        except Exception as e:
            raise e


    async def eval_function(self, ctx, code):
        if "import os" in code or "import sys" in code:
            return
        if ctx.author.id not in self.allowed_list:
            print(ctx.author.id)
            return await ctx.send("Due to security risks, this command is limited for a very small ammount of trusted users to use.")

        code = code.strip('` ') # https://github.com/Rapptz/RoboDanny/blob/master/cogs/admin.py#L57

        env = {
            'bot': self.client,
            'client': self.client,
            'ctx': ctx,
            'message': ctx.message,
            'server': ctx.message.guild,
            'guild': ctx.message.guild,
            'channel': ctx.message.channel,
            'author': ctx.message.author}
        env.update(globals()) # https://docs.python.org/3.5/library/functions.html#exec because this takes globals
        # last 2 lines where https://github.com/Run1e/AceBot/blob/rewrite/cogs/owner.py#L367-L380

        new_forced_async_code = f'async def code():\n{textwrap.indent(code, "    ")}' # https://stackoverflow.com/a/20158442/10630328 this is so i can use await on the d.py functions

        exec(new_forced_async_code, env) # compiles and runs the code, adds the returned object to env
        code = env['code'] # transforms the object into a function that is useable

        try:
            await code() # runs the function
        except Exception:
            await ctx.send(f'```{traceback.format_exc()}```') # https://stackoverflow.com/a/3702847/10630328

    @commands.Cog.listener()
    async def on_ready(self):
        self.commands = []
        for command in self.client.commands:
            self.commands.append(command.name)
            for alias in command.aliases:
                self.commands.append(alias)

        print('Debug Commands Cog Loaded')

    @commands.command(aliases=['pong'])
    async def ping(self, ctx):
        await ctx.send(f'ping? {round(self.client.latency * 1000)}ms')

    @commands.command()
    async def ip(self, ctx):
        shit = await ctx.send('w8')
        commandwhat = subprocess.Popen(['curl', 'https://ipinfo.io/ip'], stdout=subprocess.PIPE, universal_newlines=True)
        output = commandwhat.communicate()[0]
        await shit.edit(content=output)
        print(output)
        self.ip.public = output

    @commands.command(name='eval')
    async def _eval(self, ctx, *, code: str):
        await self.eval_function(ctx, code)

    @commands.command(aliases=['evaljs'])
    async def js_was_a_mistake(self, ctx, *, code: str):
        if ctx.author.id not in self.allowed_list:
            print(ctx.author.id)
            return await ctx.send("Due to security risks, this command is limited for a very small ammount of trusted users to use.")

        try:
            x = dukpy.evaljs(code)
        except dukpy.JSRuntimeError as error:
            return await ctx.send(f"```{error}```")

        await ctx.send(f"```{x}```")

    @commands.command()
    async def source(self, ctx, *, command: str = None):
        source_url = 'https://gitlab.com/nitsuga5124/nitsugabot'
        branch = 'master'
        if command is None:
            return await ctx.send(source_url)


        obj = self.client.get_command(command.replace('.', ' '))
        if obj is None:
            return await ctx.send('Could not find command.')

        src = obj.callback.__code__
        module = obj.callback.__module__
        filename = src.co_filename

        lines, firstlineno = inspect.getsourcelines(src)
        if not module.startswith('discord'):
            location = os.path.relpath(filename).replace('\\', '/')
        else:
            location = module.replace('.', '/') + '.py'
            source_url = 'https://github.com/Rapptz/discord.py'

        final_url = f'<{source_url}/blob/{branch}/{location}#L{firstlineno}>'
        await ctx.send(final_url)

    @commands.command()
    async def todo(self, ctx):
        await ctx.send('\
```markdown\n\
Current todo list is:\n\n\
WaifuBot:\n\
- Add support for this thing: https://iqdb.org/ (basically a sauce command)\n\
- Implement IDOL Complex\n\
- Banlist tags for explicit posts (loli, shota, guro, extreme_content, etc...)\n\
- Excluded tags/commands.\n\
- Tag alias check\n\n\
Music:\n\
- Add playlist support\n\
- Add an embed remover for the links. (When d.py 1.3.0 releases)\n\
- Switch to Lavalink\n\n\
- Youtube Video Notificactions.(Wintergatan, Davie504 and -GN)\n\
- Reminders\n\
- Coin, Weights, Lengths, etc... Converters\n\
- PP/New_PP For the recent command\n\
- PP Calculator for maps (like tillerino)\n\
- When mikuia api gets fully rewritten, add -GN\'s mikuia commands to the bot.\n\
- Implement search for nhentai\n\
```')

    @commands.command()
    async def invite(self, ctx):
        await ctx.send('https://discordapp.com/api/oauth2/authorize?client_id=551759974905151548&permissions=993914321&scope=bot')

    @commands.command(aliases = ['longhelp', 'lhelp', 'fullhelp', 'full_help', 'all_commands', 'allcommands'])
    async def long_help(self, ctx):
        with open('full_help.json') as help:
            jsonhelp = json.load(help)
            embedhelp = discord.Embed.from_dict(jsonhelp)
            await ctx.send('This message will be automatically deleted in 4 minutes', embed = embedhelp, delete_after=240)
            if ctx.guild == None:
                return
            else:
                await ctx.message.delete()

    async def embed_builder(self, filename):
        with open(filename) as help:
            jsonhelp = json.load(help)
        embedhelp = discord.Embed.from_dict(jsonhelp)
        return embedhelp

    @commands.command(aliases = ['commands'])
    async def help(self, ctx, category=None):
        if category is None:
            embed = await self.embed_builder('help_base.json')
            self.client.help = await ctx.send(embed = embed, content = 'Run `delete_help` to delete this message')
        elif category == 'nsfw':
            embed = await self.embed_builder('help_nsfw_base.json')
            self.client.help = await ctx.send(embed = embed, content = 'Run `delete_help` to delete this message')
        elif category == 'boorus':
            embed = await self.embed_builder('help_booru_list.json')
            self.client.help = await ctx.send(embed = embed, content = "Extra boorus: <https://pastebin.com/qdDe8Us0>")
            #self.client.help = await ctx.send(embed = embed, content = """
#(allgirl | Command aliases: ['allgirlbooru', 'ag', 'agbooru', 'agb'] >>> allgirl.booru.org)
#(drawfriends | Command aliases: ['dw', 'drawd', 'dfriends', 'sketch', 'sketches'] >>> drawfriends.booru.org)
#(dom | Command aliases: ['dombooru', 'domb', 'femdom'] >>> dom.booru.org)
#(scat | Command aliases: ['scatb', 'scatbooru', 'shit'] >>> scat.booru.org)
#(catgirls | Command aliases: ['cg', 'neko', 'nekos', 'catg', 'kats', 'kat'] >>> catgirls.booru.org)
#(male | Command aliases: ['man', 'malebooru', 'mb'] >>> m.booru.org)
#(footbooru | Command aliases: ['foot', 'feetbooru', 'fb'] >>> footfetishbooru.booru.org)
#(vpokemon | Command aliases: ['virtualpokemon', 'pokemon', 'vp'] >>> vp.booru.org)
#(pokeymans | Command aliases: ['pokemons', 'pm', 'pokeym'] >>> pokeymans.booru.org)
#(censored | Command aliases: ['why', 'censor', 'cb'] >>> censored.booru.org)
#(red | Command aliases: ['redbooru'] >>> red.booru.org)
#(inflatebooru | Command aliases: ['inflate', 'inflationbooru', 'ib', 'inflationb', 'inflateb'] >>> inflatebooru.booru.org)
#(wolf | Command aliases: ['wolfy', 'wolfbooru', 'wb'] >>> wolf.booru.org)
#(bunnie | Command aliases: ['trapbooru', 'bulgebooru', 'bulge', 'bulgeb', 'bunniebooru', 'bb'] >>> bunnie.booru.org)
#(futabooru | Command aliases: ['futa', 'futab', 'fbooru'] >>> futabooru.booru.org)
#(vulva | Command aliases: ['vagina', 'vaginal', 'vulvabooru', 'vagiabooru', 'vaginab', 'vulvab'] >>> vulva.booru.org)
#(g | Command aliases: ['darkbooru', 'simplebooru', 'dark', 'simple', 'darkb', 'simpleb'] >>> g.booru.org)
#(closeup | Command aliases: ['closeupb', 'closeupbooru', 'vaginacloseup'] >>> closeup.booru.org)
#(sai | Command aliases: ['saibooru', 'saib'] >>> sai.booru.org)
#(clop | Command aliases: ['mlp', 'clopb', 'clopbooru', 'mlpb', 'mlpbooru', 'pony', 'ponyb', 'mylittlepony', 'ponybooru'] >>> clop.booru.org)
#(size | Command aliases: ['extrasize', 'sizeb', 'sizebooru', 'macrobooru', 'macrob', 'macro', 'microbooru', 'microb', 'micro', 'small', 'large', 'magnification', 'fat', 'supersize', 'obese'] >>> size.booru.org)
#(joi | Command aliases: ['joiboou', 'joib'] >>> joi.booru.org)
#(pai | Command aliases: ['paiboou', 'paib', 'paipan'] >>> pai.booru.org)
#""")
        elif category == 'osu':
            embed = await self.embed_builder('help_osu.json')
            self.client.help = await ctx.send(embed = embed, content = 'Run `delete_help` to delete this message')
        elif category == 'tags' or category == 'tag':
            embed = await self.embed_builder('help_tags.json')
            self.client.help = await ctx.send(embed = embed, content = 'Run `delete_help` to delete this message')
        else:
            self.client.help = await ctx.send('Currently that parameter is not supported.\nRun `delete_help` to delete this message')

    @commands.command()
    async def permissions(self, ctx, who:discord.Member=None):
        if who is None:
            #perms = ctx.guild.me.guild_permissions
            perms = ctx.author.guild_permissions
        else:
            perms = who.guild_permissions

        paginator = commands.Paginator()
        max_perm_length = max(len(p[0]) for p in perms) + 1

        for attr, value in perms:
            indicator = '✅' if value else '❌'
            paginator.add_line(f'{attr: <{max_perm_length}} --> {indicator}')

        for page in paginator.pages:
            await ctx.send(page)

    @commands.command()
    async def profile(self, ctx, member:discord.Member=None):
        await ctx.send('Did you mean `osuprofile` ?')


    @commands.command(aliases = ['deletehelp'])
    async def delete_help(self, ctx):
        try:
            await self.client.help.delete()
            try:
                await ctx.message.delete()
            except:
                await ctx.send(content = 'I recommend you delete the command to avoid having useless messages in chat.', delete_after=10)
        except:
            await ctx.send('Could not find a help command.')



    @commands.Cog.listener()
    async def on_command_error(self, ctx, error):
        if isinstance(error, commands.errors.CommandNotFound):
            wrong_command = ctx.invoked_with
            similar = []
            for command in self.commands:
                if wrong_command in command or command in wrong_command:
                    if len(command) > 1:
                        similar.append(command)
            equal = difflib.get_close_matches(wrong_command, self.commands, n=100, cutoff=0.8)
            for item in equal:
                similar.append(item)

            similar = list(set(similar))

            similar = ", ".join(map(str, similar))


            if similar != "[]":
                await ctx.send(f"{error}.\nDid you mean: `{similar}`")
            else:
                await ctx.send(f"{error}")
        else:
            await ctx.send(f'Code error: ||{error}||')
            traceback.print_exception(type(error), error, error.__traceback__, file=sys.stderr)

def setup(client):
    client.add_cog(Debug(client))

