import asyncio
import aiohttp
import random
import discord
from discord.ext import commands

class nhentai(commands.Cog):
    def __init__(self, client):
        self.client = client

    @commands.Cog.listener()
    async def on_ready(self):
        print('nhentai Cog Loaded')

    async def prev_page(self):
        self.num -= 1
        return
    async def next_page(self):
        self.num += 1
        return

    def reacted_check(self, payload):
        if payload.user_id == self.client.user.id:
            return False
        if payload.message_id != self.msg.id:
            return False

        to_check = str(payload.emoji)
        reaction_emojis = [('⬅️', self.prev_page), ('➡️', self.next_page)]
        for (emoji, func) in reaction_emojis:
            if to_check == emoji:
                self.match = func
                return True
        return False

    async def nhentai_send_post(self, ctx, id, media_id, page, pages, favs, title, ptitle, page_data, edit):
        if page_data[page-1]['t'] == 'j':
            format = "jpg"
        elif page_data[page-1]['t'] == 'p':
            format = "png"
        else:
            format = "jpg"

        embed = discord.Embed(title=title, colour=discord.Colour(int(id)), url=f"https://nhentai.net/g/{id}/")
        embed.set_footer(text=ptitle)
        embed.add_field(name="Favs", value=str(favs))
        embed.set_image(url=f"https://i.nhentai.net/galleries/{media_id}/{page}.{format}")
        embed.add_field(name="Page", value=f"{page}/{pages}")
        if edit is False:
            self.msg = await ctx.send(content=f"https://nhentai.net/g/{id}/", embed=embed)
            await self.msg.add_reaction('⬅️')
            await self.msg.add_reaction('➡️')
        else:
            await self.msg.edit(content=f"https://nhentai.net/g/{id}/", embed=embed)

    @commands.command(aliases = ['nh', 'manga', 'doujinshi'])
    async def nhentai(self, ctx, parameter=None):
        if ctx.channel is not None:
            try:
                if not ctx.channel.is_nsfw():
                    return await ctx.send('You are only allowed to use this command in NSFW Channels or DM\'s')
            except AttributeError:
                pass
        if parameter is None:
            return await ctx.send("https://nhentai.net/")

        session = aiohttp.ClientSession()
        if parameter == "random":
            async with session.get('https://nhentai.net/random/') as data:
                url = str(data.url)
                id = str(data.url).split('/')[4]
        else:
            url = f"https://nhentai.net/g/{parameter}"
            id = parameter

        self.client.reload_extension('cogs.nhentai')

        api_url = f"https://nhentai.net/api/gallery/{id}"

        async with session.get(api_url) as url:
            data = await url.json()

        self.num = 1
        favs = data['num_favorites']
        title = data['title']['english']
        ptitle = data['title']['pretty']
        pages = data['images']['pages']
        media_id = data['media_id']

        await self.nhentai_send_post(ctx, id, media_id, self.num, len(pages), favs, title, ptitle, pages, edit=False)
        self.ctx = ctx

        for _ in range(0, 1000):
            done, pending = await asyncio.wait([
                self.client.wait_for('raw_reaction_add', check=self.reacted_check),
                self.client.wait_for('raw_reaction_remove', check=self.reacted_check)
                ], return_when=asyncio.FIRST_COMPLETED, timeout=240)
            if not done:
                for future in pending:
                    future.cancel()
                try:
                    await self.msg.clear_reactions()
                except:
                    pass
                finally:
                        break

            try:
                payload = done.pop().result()
            except:
                for future in pending:
                    future.cancel()
                try:
                    await self.msg.clear_reactions()
                except:
                    pass
                finally:
                    break
            for future in pending:
                future.cancel()

            try:
                await self.msg.remove_reaction(payload.emoji, discord.Object(id=payload.user_id))
            except:
                pass 

            await self.match()
            await self.nhentai_send_post(ctx, id, media_id, self.num, len(pages), favs, title, ptitle, pages, edit=True)

        await session.close()


def setup(client):
    client.add_cog(nhentai(client))

