import os
from configparser import ConfigParser
import discord
from discord.ext import commands
from pybooru import Danbooru
from termcolor import colored

configp = ConfigParser()
configp.read('stuff.ini')

TOKEN = configp.get('TOKENS', 'Discord Token')
danbooru_site = Danbooru('danbooru', username=configp.get('TOKENS', 'Danbooru Username'), api_key=configp.get('TOKENS', 'Danbooru Token'))
osu_token = configp.get('TOKENS', 'osu! Token')

client = commands.Bot(command_prefix = ['food!', ': food!', ':food!', 'n!', ': n!', ':n!', 'test!', ': test!', ':test!', 'm!', ': m!', ':m!', '[', ': [', ':[', ']', ': ]', ':]'])

client.remove_command('help')

client.configp = configp
client.danbooru_site = danbooru_site
client.osu_token = osu_token
client.xml_cog = False

@client.event
async def on_ready():
    print(colored('Logged in as', 'magenta'))
    print(colored(f'Username: {client.user.name}', 'green'))
    print(colored(f'ID: {client.user.id}', 'green'))
    print(colored(f'In {len(client.guilds)} guilds', 'cyan'))
    print(colored('-------', 'magenta'))
    await client.change_presence(activity=discord.Activity(type=discord.ActivityType.watching, name='Slof being a qt'), status=None, afk=False)

##########################################
#join and leave a server

@client.event
async def on_member_join(member):
    print(f'+++ {member} has joined.')
@client.event
async def on_member_remove(member):
    print(f'--- {member} has left.')

##########################################
#Cogs commands


@client.command()
async def load(ctx, extension):
    if extension == "xmlbooru":
        client.xml_cog = True
    client.load_extension(f'cogs.{extension}')
    print(f'Loaded {extension}')
    await ctx.send(f'Loaded {extension}')

@client.command()
async def unload(ctx, extension):
    client.unload_extension(f'cogs.{extension}')
    print(f'Unloaded {extension}')
    await ctx.send(f'Unloaded {extension}')

@client.command(aliases = ['rl'])
async def reload(ctx, *extensions):
    for extension in extensions:
        if extension == "xmlbooru":
            client.xml_cog = True
        client.unload_extension(f'cogs.{extension}')
        client.load_extension(f'cogs.{extension}')
        print(f'Reloaded {extension}')
        await ctx.send(f'Reloaded {extension}')

#if __name__ == "__main__":
for filename in os.listdir('./cogs'):
    if filename.endswith('.py'):
        if filename[:-3] != "mobooru":
            if filename[:-3] == "xmlbooru":
                client.xml_cog = True
            client.load_extension(f'cogs.{filename[:-3]}')


client.run(TOKEN, bot=True)
