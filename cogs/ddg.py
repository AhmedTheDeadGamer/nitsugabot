import json
import requests
import discord
from discord.ext import commands

class DDG(commands.Cog):
    def __init__(self, client):
        self.client = client

    @commands.Cog.listener()
    async def on_ready(self):
        print('DuckDuckGo Cog Loaded')


    async def duckduckgoapi(self, ctx, output):
        baseurlR = f"https://api.duckduckgo.com/?q={output}&format=json&pretty=1&no_redirect=1&atb=v141-1"
        baseurl = f"https://api.duckduckgo.com/?q={output}&format=json&pretty=1&no_html=1&skip_disambig=1&atb=v141-1"

        ddgR_json = requests.get(baseurlR)
        ddg_json = requests.get(baseurl)

        if output.startswith('!'):
            ddgR = json.loads(ddgR_json.content)

            if ddgR['Redirect'] != '':
                await ctx.send(f"<{ddgR['Redirect']}>")
            else:
                await ctx.send('Bang not found.')

            return

        ddg = json.loads(ddg_json.content)
        #print(ddg)

        if 'https' in str(ddg):
            url = ddg['AbstractURL']
            description = ddg['AbstractText']
            icon_url = ddg['Image']

            embed = discord.Embed(title="Link", colour=discord.Colour(0xff641d), url=url, description=description)
            embed.set_author(name="Search result", url=url, icon_url=icon_url)

            await ctx.send(embed=embed)

        else:
            await ctx.send(f'https://duckduckgo.com/?q={output}')
            return





    @commands.command(aliases = ['s', 'sh'])
    async def search(self, ctx, *args):
        if args == ():
            await ctx.send('Imagine trying to search absolutely nothing 🤔')
            return

        output = ''
        for word in args:
            output += word
            output += '+'
        output = output[:-1]

        #print(output)
        await self.duckduckgoapi(ctx, output)

    @commands.command()
    async def noresults(self, ctx):
        await ctx.send('It\'s very well explained on the Second and Third Paragraphs on <https://api.duckduckgo.com/api>.\ntl;dr: It\'s not legal for DuckDuckGo to use stuff they don\'t own like the information inside websites unless the website owner specifically allows them.\nHopefully i will be able to fix this on a future day by using the same method they use to get results normally.')

    @commands.command(aliases = ['bang'])
    async def bangs(self, ctx):
        await ctx.send('You can find a list of bangs (!) on here\n<https://duckduckgo.com/bang/>')

    @commands.command()
    async def google(self, ctx):
        await ctx.send('use the bang `!g` on the search command')

def setup(client):
    client.add_cog(DDG(client))

