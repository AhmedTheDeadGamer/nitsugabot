import discord
from discord.ext import commands
import aiohttp
import aiofiles
import json
import re
import random

class behoimi(commands.Cog):
    def __init__(self, client):
        self.client = client

    @commands.Cog.listener()
    async def on_ready(self):
        print('Behoimi Cog Loaded')

    async def check_nsfw(self, ctx):
        self.client.is_nsfw = ctx.guild is None or ctx.channel.is_nsfw()

    async def set_data(self, post):
        preset_data = [post['rating']]
        preset_data.append(post['score'])
        preset_data.append(post['source'])
        preset_data.append(post['file_url'])
        preset_data.append(post['sample_url'])
        self.client.last_md5 = post['md5']

        return preset_data
    
    async def send_post(self, ctx, preset_data):
        rating = preset_data[0]
        score = preset_data[1]
        source = preset_data[2]
        full_size = preset_data[3]
        sample = preset_data[4]

        if rating == 's':
            rating = 'Safe'
        elif rating == 'q':
            rating = 'Questionable'
        elif rating == 'e':
            rating = 'Explicit'

        embed = discord.Embed(colour=discord.Colour(0x222222))

        async with self.session.request("GET", sample, headers=self.header) as url: #opens the url as url
            if url.status == 200:
                filename = sample.split('/')[-1]
                self.client.filename = filename
                f = await aiofiles.open(filename, mode='wb')
                await f.write(await url.read())

        filename = discord.File(self.client.filename)

        embed.set_image(url=f'attachment://{self.client.filename}')

        embed.add_field(name="Rating", value=rating, inline=True)
        embed.add_field(name="Score", value=score, inline=True)
        if source != '':
            if 'http' in source:
                embed.add_field(name="Source", value=f"[Here]({source})", inline=True)
            else:
                embed.add_field(name="Source", value=source, inline=True)

        self.client.image = await ctx.send(embed=embed, file=filename)
        self.client.source = None
        await self.session.close()


    async def api_call(self, tags, page=0):
        self.url = f'http://behoimi.org/post/index.json?tags={tags}&page={str(page)}&limit=50'
        self.header = {
                "User-Agent" : "Mozilla/5.0 (X11; Linux x86_64; rv:68.0) Gecko/20100101 Firefox/68.0",
                "Connection": "Keep-Alive",
                "Referer" : self.url,
                "Cookie" : {
                    "login" : "nitsuga5124",
                    "pass_hash": self.client.bhm_pass_hash,
                    "blacklisted_tags":"",
                    "~three-dee-booru~": self.client.bhm_3db
                    }
                }
        async with self.session.request("GET", self.url, headers={'User-Agent' : "Mozilla/5.0 (X11; Linux x86_64; rv:68.0) Gecko/20100101 Firefox/68.0", "Connection": "Keep-Alive"}) as url: #opens the url as url
        #async with aiohttp.request("GET", url) as url: #opens the url as url
            data = await url.read() #gets content
            #print(url)
            data = json.loads(data)
            return data


    async def page_loop(self, ctx, data, tags):
        if data == []:
            await ctx.send('One of the tags doesn\'t exist.')
            return 727

        page = 0
        while len(data) == 50:
            page += 1
            data = await self.api_call(tags, page)

            if page == 5:
                break
        self.client.datalen = len(data)

        if page < 2:
            pagef = 0
        else:
            pagef = 2
        page += 1
        page = random.choice(range(pagef, page))
        return page

    async def pick_post(self, tags, page):
        data = await self.api_call(tags, page)
        post_num = random.choice(range(0, self.client.datalen))
        post = data[post_num]
        return post
    
    async def functions(self, ctx, tags):
        data = await self.api_call(tags)
        page = await self.page_loop(ctx, data, tags)
        if page == 727:
            return
        post = await self.pick_post(tags, page)
        preset_data = await self.set_data(post)
        await self.send_post(ctx, preset_data)

    @commands.command(aliases = ['bhimi', 'bhm', '3dbooru', '3db'])
    async def behoimi(self, ctx, flag=None, *args):
        await self.check_nsfw(ctx)
        self.session = aiohttp.ClientSession()

        output = ''
        for word in args:
            output += word
            output += '%20'
            if 'rating' in output:
                if self.client.is_nsfw is True:
                    await ctx.send('You are not allowed to use custom ratings outside nsfw channels or dm\'s')
                    return
        output = re.sub("\s", "", (output.lower()))[:-3]

        if flag is None:
            output += '%20rating:S'
            await self.functions(ctx, output)
        elif flag == '-r':
            if self.client.is_nsfw is True:
                await self.functions(ctx, output)
            else:
                await ctx.send('You are not allowed to use that flag outside nsfw channels or dm\'s')
        elif flag == '-n':
            if self.client.is_nsfw is True:
                choice = random.choice(['E', 'Q'])
                output += f'%20rating:{choice}'
                await self.functions(ctx, output)
            else:
                await ctx.send('You are not allowed to use that flag outside nsfw channels or dm\'s')
        elif flag == '-x':
            if self.client.is_nsfw is True:
                output += '%20rating:E'
                await self.functions(ctx, output)
            else:
                await ctx.send('You are not allowed to use that flag outside nsfw channels or dm\'s')
        elif flag == '-u':
            output = f'rating:S%20user:{output}'
            await self.functions(ctx, output)

            return
        else:
            if 'rating' in flag:
                if self.client.is_nsfw is True:
                    await ctx.send('You are not allowed to use custom ratings outside nsfw channels or dm\'s')
                    return
                else:
                    output += f'%20{flag}'
            else:
                if 'rating' in output:
                    output += f'%20{flag}'
                else:
                    output += f'%20rating:S%20{flag}'
            await self.functions(ctx, output)

def setup(client):
    client.add_cog(behoimi(client))
