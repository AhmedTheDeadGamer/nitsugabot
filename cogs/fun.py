import discord
from discord.ext import commands
import re
from sympy import sympify
from googletrans import Translator
import random

class Fun(commands.Cog):
    def __init__(self, client):
        self.client = client

    async def text_to_bits(self, text, encoding='ISO-8859-1', errors='surrogatepass'):
        bits = bin(int.from_bytes(text.encode(encoding, errors), 'big'))[2:]
        return bits.zfill(8 * ((len(bits) + 7) // 8))

    async def text_from_bits(self, bits, encoding='ISO-8859-1', errors='surrogatepass'):
        n = int(bits, 2)
        return n.to_bytes((n.bit_length() + 7) // 8, 'big').decode(encoding, errors) or '\0'


    @commands.Cog.listener()
    async def on_ready(self):
        print('Fun Commands Cog Loaded')

    @commands.command(description = 'food!define {term}', aliases = ['ub', 'urabndictionary', 'udictionary', 'udefine'])
    async def define(self, ctx, *args):
        baseurl = "https://www.urbandictionary.com/define.php?term="
        output = ''
        for word in args:
            output += word
            output += '%20'
        output = re.sub("\s","",(output.lower()))[:-3]
        await ctx.send(baseurl + output)
    @commands.command(description = 'food!dictionary {word}', aliases = ['dic'])
    async def dictionary(self, ctx, word):
        await ctx.send(f'Source1: https://www.merriam-webster.com/dictionary/{word}')
        await ctx.send(f'Source2: <https://www.wordnik.com/words/{word}>')

    @commands.command(description = 'food!report <who> <reason>')
    async def report(self, ctx, who, *, args):
        await ctx.send("Ok, Reported {who} for {args}")

    @commands.command()
    async def question(self, ctx, *, question):
        if "nitsuga" in question.lower():
            if "gay" in question.lower() or "homosexual" in question.lower():
                if "not" in question.lower():
                    ans = "Id say he's 50% there"
                else:
                    ans = "50% Gay"
            elif " bi" in question.lower() or "bisexual" in question.lower():
                if "not" in question.lower():
                    ans = "No"
                else:
                    ans = "Yes"
            else:
                ans = random.choice(['Yes', 'no'])
        elif "jaroda" in question.lower() or "slof" in question.lower() or "nene" in  question.lower():
            if "not" in question.lower():
                ans = "No"
            else:
                ans = "Yes"
        else:
            ans = random.choice(['Yes', 'no'])

        await ctx.send(f"Question: {question}\nAnswer: {ans}")

    @commands.command(aliases = ['calc'])
    async def calculator(self, ctx, *, maths):
        message = sympify(maths)
        message = await commands.clean_content().convert(ctx, message)
        await ctx.send(re.sub('@', '＠', message))
    @commands.command(aliases = ['calc2'])
    async def calculator2(self, ctx, *, maths):
        message = str(maths)
        message = await commands.clean_content().convert(ctx, message)
        await ctx.send(message)

    @commands.command()
    async def hug(self, ctx, member:discord.Member=False):
        if member:
            return await ctx.send(f"{ctx.author.mention} hugs {member.mention} <:slof:422900553706766357>")
        else:
            return await ctx.send(f"{ctx.author.mention} hugs {self.client.user.mention} <:slof:422900553706766357>")

    @commands.command()
    async def encrypt(self, ctx, *, text):
        x = await self.text_to_bits(text)
        lx = len(x)

        y = int(x,2) << 1
        y = bin(y)
        ly = len(y)

        diff = "".join("0" for x in range(lx - ly))
        y = diff+y

        d = await self.text_from_bits(y)
        _hex = hex(int.from_bytes(d.encode('ISO-8859-1', 'surrogatepass'), 'big'))
        _dec = int(_hex, 16)
        _bin = bin(int.from_bytes(d.encode('ISO-8859-1', 'surrogatepass'), 'big'))

        await ctx.send(f"```Hex Base16 : {_hex}\nDec Base10 : {_dec}\nBin Base2 : {_bin}```")

    @commands.command()
    async def decrypt(self, ctx, raw, base:int=0):
        if base == 0:
            if raw.startswith('0x'):
                value = 16
            elif raw.startswith('0b'):
                value = 2
            else:
                value = 10
        else: value = base

        y = int(raw,value) >> 1
        y = bin(y)

        await ctx.send(await self.text_from_bits(y))


    @commands.command(aliases = ['tl'])
    async def translate(self, ctx, lang=None, *, text=None):
        if lang is None:
            return await ctx.send("Usage: `translate jp hello world!`")
        if text is None:
            return await ctx.send("Please, put some text to translate to the specified language")

        if lang == 'jp':
            lang = 'ja'
        print(text)
        if "source=" in text or "from=" in text or "src=" in text:
            text_list = text.split()
            text = ""
            for word in text_list:
                if "source=" in word or "from=" in word or "src=" in word:
                    print(word)
                    source = word.split('=')[1]
                else:
                    text += word

            translator = Translator()
            translation = translator.translate(text, src=source, dest=lang)

            if translation.pronunciation is not None:
                await ctx.send(f"From {translation.src}:\n{translation.text}\n\nPronounciation: {translation.pronunciation}")
            else:
                await ctx.send(f"From {translation.src}:\n{translation.text}")

        else:
            translator = Translator()
            translation = translator.translate(text, dest=lang)
            await ctx.send(f"From {translation.src}:\n{translation.text}\n\nPronounciation: {translation.pronunciation}")





def setup(client):
    client.add_cog(Fun(client))

