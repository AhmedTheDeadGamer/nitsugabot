import discord
from discord.ext import commands
import os
import random
import re
import urllib.request
import json
import requests

class MoBooru(commands.Cog):
    def __init__(self, client):
        self.client = client
        

    @commands.Cog.listener()
    async def on_ready(self):
        print('MoBooru Cog Loaded')


    
    async def booru_send(self, ctx, post):
        baseurl = f'http://danbooru.donmai.us/posts/{post["source_id"]}'
        baseurl = baseurl.rstrip()

        danbooru_json = requests.get(f'https://danbooru.donmai.us/posts/{post["source_id"]}.json')
        #print(f'https://danbooru.donmai.us/posts/{post["source_id"]}.json')
        danbooru = json.loads(danbooru_json.content)
        #print(danbooru)
        filesource = danbooru["source"]
        if self.client.isnsfw == True:
            if ctx.guild == None or ctx.channel.is_nsfw():
                self.client.image = await ctx.send(file=discord.File(self.client.filename))
                self.client.source = await ctx.send(f'Source: <{filesource}>\nOriginal Post: <{baseurl}>')
                os.remove(self.client.filename)
            else:
                await ctx.send('You can\'t use explicit tags outside nsfw channels.')

        elif self.client.isnsfw == False:
            self.client.image = await ctx.send(file=discord.File(self.client.filename))
            self.client.source = await ctx.send(f'Source: <{filesource}>\nOriginal Post: <{baseurl}>')
            os.remove(self.client.filename)

    async def mobooru_download(self, ctx, post):
        #print(post['source_preview_url'])

        #https://cdn.mobooru.com/samples/ https://raikou4.donmai.us/preview/
        #https://mobooru.com/api/image/

        #pick = post['source_preview_url']
        #pick = re.sub('https://raikou4.donmai.us/preview/', 'https://cdn.mobooru.com/samples/', pick)
        #https://mobooru.com/api/image/{query}.{filetype}

        pickq = post['query']
        pickf = post['filetype']
        pick = f'https://mobooru.com/api/image/{pickq}.{pickf}'
        #print(f'pick: {pick}')

        req = urllib.request.Request(pick, headers={'User-Agent' : "Magic Browser"})
        con = urllib.request.urlopen(req)
        heximage = con.read()

        filename = f'{pickq}.{pickf}'
        with open(filename, 'wb') as image:
            image.write(heximage)
        self.client.filename = filename

        await self.booru_send(ctx, post)




    async def mobooru_random(self, ctx, tags, posts, page, rating):
        token = self.client.configp.get("TOKENS", "Mobooru Token")
        page = random.randint(0,self.client.pages)
        #print(f'page: {page}')
        self.client.mobooru_site = f'https://mobooru.com/api/search/{tags}/{posts}/{page}?token={token}&history=false&filters={rating}'
        mobooru_json = requests.get(self.client.mobooru_site)
        mobooru = json.loads(mobooru_json.content)
        #print(len(mobooru['result']))
        post = random.choice(mobooru['result'])
        #print(mobooru['pipeline'])
        #print(mobooru['params']['query'])
        #print(post)
        #self.client.post = post
        await self.mobooru_download(ctx, post)

    async def mobooru_site(self, ctx, tags, posts, page, rating):
        token = self.client.configp.get("TOKENS", "Mobooru Token")
        self.client.mobooru_site = f'https://mobooru.com/api/search/{tags}/{posts}/{page}?token={token}&history=false&filters={rating}'
        
        mobooru_json = requests.get(self.client.mobooru_site)
        mobooru = json.loads(mobooru_json.content)

        #print(len(mobooru['result']))
        #print(mobooru['executionTime'])
        #print(mobooru['success'])

        if len(mobooru['result']) == 0:
            print('tag doesn\'t exist?')
            await ctx.send('One of the tags doesn\'t exist.')
            return


        async def pageloop(self):
            self.client.pages = 0
            while len(mobooru['result']) == int(posts):
                self.client.pages = str(self.client.pages)
                self.client.mobooru_site = f'https://mobooru.com/api/search/{tags}/{posts}/{self.client.pages}?token={token}&history=false&filters={rating}'
                #if len(mobooru['result']) != 99:
                #    break
                self.client.pages = int(self.client.pages)
                self.client.pages += 1

                if self.client.pages == 10:
                    break

            #print(self.client.pages)

        await pageloop(self) #this 2 lines are because the api is slow
        #self.client.pages = 0

        await self.mobooru_random(ctx, tags, posts, page, rating)




    @commands.command(aliases = ['bg'], description = 'food!mbestgirl set tag\nfood!mbestgirl from <user-ping>\nfood!mbestgirl (flag) tags\nfood!mbestgirl tags')
    async def bestgirl(self, ctx, flag, *args):
        try:
            self.client.configp.read('stuff.ini')
            if flag == 'set':
                output =''
                for word in args:
                    output += word
                    output += '_'
                output = re.sub("\s","",(output.lower()))[:-1]
                print(ctx.author.id)
                print(*args)
                if '@' in ctx.message.content:
                    await ctx.send(f'You are not allowed to ping using me... <@{ctx.message.author.id}>')
                elif 'loli' in ctx.message.content:
                    await ctx.send('Discord doesn\'t allow lolis nor shota. If you feel like this was an error, ping nitsuga5124#2207')
                #elif 'lolicon' in ctx.message.content:
                #    await ctx.send('Discord doesn\'t allow lolis nor shota. If you feel like this was an error, ping nitsuga5124#2207')
                elif 'shota' in ctx.message.content:
                    await ctx.send('Discord doesn\'t allow lolis nor shota. If you feel like this was an error, ping nitsuga5124#2207')
                #elif 'shotacon' in ctx.message.content:
                #    await ctx.send('Discord doesn\'t allow lolis nor shota. If you feel like this was an error, ping nitsuga5124#2207')
                elif '-GN' in ctx.message.content:
                    await ctx.send('https://tenor.com/view/culture-man-of-culture-cultured-gif-10903367')
                    self.client.configp.set('bestgirl', str(ctx.author.id), output)
                    with open('stuff.ini', 'w') as stufffile:
                        self.client.configp.write(stufffile)
                    await ctx.send(f'Waifu set to {output}!')
                elif 'minusGN' in ctx.message.content:
                    await ctx.send('https://tenor.com/view/culture-man-of-culture-cultured-gif-10903367')
                    self.client.configp.set('bestgirl', str(ctx.author.id), output)
                    with open('stuff.ini', 'w') as stufffile:
                        self.client.configp.write(stufffile)
                    await ctx.send(f'Waifu set to {output}!')
                elif 'clear' in ctx.message.content:
                    output = ''
                    self.client.configp.set('bestgirl', str(ctx.author.id), output)
                    with open('stuff.ini', 'w') as stufffile:
                        self.client.configp.write(stufffile)
                    await ctx.send(f'You just broke up with your waifu :(')
    
                else:
                    self.client.configp.set('bestgirl', str(ctx.author.id), output)
                    with open('stuff.ini', 'w') as stufffile:
                        self.client.configp.write(stufffile)
                    await ctx.send(f'Waifu set to {output}!')
    
    
            elif flag == 'from':
                output = ''
                for word in args:
                    output += word
                print(output)
                user = re.sub('[<>@]','', output)
                print(user)
                waifu = self.client.configp.get('bestgirl', str(user))
                if waifu == '':
                    await self.ctx.send('He doesn\'t have one, he lives on the edge 😎')
                    return
    
                await ctx.send(f'{waifu}!')
    
                output = f'{waifu}'
                self.client.isnsfw = False
                tags = output
                posts = '50'
                page = '0'
                rating = 's'
                await self.mobooru_site(ctx, tags, posts, page, rating)


            else: #mbestgirl with tags
                waifu = self.client.configp.get('bestgirl', str(ctx.message.author.id))
                if waifu == '':
                    await ctx.send('Set a waifu first with `food!mbestgirl set <tag>`')
                    return
    
                await ctx.send(f'{waifu}!')

                outputf = ''
                for word in args:
                    outputf += word
                    outputf += '%20'
                outputf = re.sub("\s","",(outputf.lower()))[:-3]
                
                if outputf == '':
                    outputf = waifu
                else:
                    outputf += f'%20{waifu}'
                output = outputf

                if flag == '-r':
                    self.client.isnsfw = True
                    tags = output
                    posts = '50'
                    page = '0'
                    rating = 'sqe'
                    await self.mobooru_site(ctx, tags, posts, page, rating)
    
                elif flag == '-x':
                    output += '%20rating:e'
                    self.client.isnsfw = True
                    tags = output
                    posts = '50'
                    page = '0'
                    rating = 'e'
                    await self.mobooru_site(ctx, tags, posts, page, rating)
    
                elif flag == '-n':
                    rating = random.choice(['e', 'q'])
                    output += f'%20rating:{rating}'
                    self.client.isnsfw = True
                    tags = output
                    posts = '50'
                    page = '0'
                    await self.mobooru_site(ctx, tags, posts, page, rating)
    
                else:
                    output += f'%20{flag}'
                    self.client.isnsfw = False
                    tags = output
                    posts = '50'
                    page = '0'
                    rating = 's'
                    await self.mobooru_site(ctx, tags, posts, page, rating)
        except:
            raise
    
    @bestgirl.error
    async def bestgilr_wo_tags(self, ctx, error):
        if isinstance(error, discord.ext.commands.errors.MissingRequiredArgument):
            self.client.configp.read('stuff.ini')
            waifu = self.client.configp.get('bestgirl', str(ctx.message.author.id))
            if waifu == '':
                await self.ctx.send('Set a waifu first with `food!mbestgirl set <tag>`')
                return
    
            await ctx.send(f'{waifu}!')
    
            output = waifu
            self.client.isnsfw = False
            tags = output
            posts = '50'
            page = '0'
            rating = 's'
            await self.mobooru_site(ctx, tags, posts, page, rating)
    
    
    @commands.command(aliases=['pic', 'p'], description = 'food!mpicture <flag> tags (-x for explicit, -r for non safe, -n for any rating)\nfood!mpicture tags (Safe only)')
    async def picture(self, ctx, flag, *args):
        output = ''
        for word in args:
            output += word
            output += '%20'
        output = re.sub("\s","",(output.lower()))[:-3]
        #output += '' #may be useless, idk
    
        try:
            if flag == '-r':
                self.client.isnsfw = True
                if output == '':
                    tags = '-dfgdfddg'
                    posts = '50'
                    page = '0'
                    rating = 'sqe'
                    await self.mobooru_site(ctx, tags, posts, page, rating)
                else:
                    tags = output
                    posts = '50'
                    page = '0'
                    rating = 'sqe'
                    await self.mobooru_site(ctx, tags, posts, page, rating)
    
            elif flag == '-x':
                if output != '':
                    output = output
                else:
                    output = 'rating:e'
                print(output)
                self.client.isnsfw = True
                tags = output
                posts = '50'
                page = '0'
                rating = 'e'
                await self.mobooru_site(ctx, tags, posts, page, rating)
    
            elif flag == '-n':
                rating = random.choice(['e', 'q'])
                if output != '':
                    output = output
                else:
                    output = f'rating:{rating}'
                self.client.isnsfw = True
                tags = output
                posts = '50'
                page = '0'
                await self.mobooru_site(ctx, tags, posts, page, rating)
    
            else:
                if output == '':
                    output = flag
                else:
                    output += f'%20{flag}'
                self.client.isnsfw = False
                tags = output
                posts = '50'
                page = '0'
                rating = 's'
                await self.mobooru_site(ctx, tags, posts, page, rating)
    
        except discord.errors.HTTPException:
            await ctx.send('File too large')
        except urllib.error.HTTPError:
            await ctx.send('File too large')
    
    @picture.error
    async def flagless_image(self, ctx, error):
        if isinstance(error, discord.ext.commands.errors.MissingRequiredArgument):
            self.client.isnsfw = False

            tags = 'rating:s'
            posts = '50'
            page = '0'
            rating = 's'
            await self.mobooru_site(ctx, tags, posts, page, rating)



    #@commands.command()

def setup(client):
    client.add_cog(MoBooru(client))

