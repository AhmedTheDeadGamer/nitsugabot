import os
import urllib.request
import urllib
import random
import hashlib
import asyncio
import discord
from discord.ext import commands

class LocalNSFW(commands.Cog):
    def __init__(self, client):
        self.client = client
        self.folders = [
            (727, 'random choice of the next'),
            (1, 'big-open gaped holes, prolapse'),
            (2, 'ecchi, ass, pussy, normal sex'),
            (3, 'feet, tights, socks, thighs, bunny girls'),
            (4, 'furry, kemono, bestiality'),
            (5, 'inflation, vore and pregnant'),
            (6, 'irl shit'),
            (7, 'monster girls and succubbus'),
            (8, 'tentacles, slimes, bugs and monsters'),
            (9, 'toys, insertions and masturbation'),
            (10, 'transformation, corruption and macro-micro'),
            (11, 'yuri and multiple girls')
        ]

    async def prev_page(self):
        self.num -= 1
        return
    async def next_page(self):
        self.num += 1
        return

    async def md5(self, fname):
        hash_md5 = hashlib.md5()
        with open(fname, "rb") as f:
            for chunk in iter(lambda: f.read(4096), b""):
                hash_md5.update(chunk)
        return hash_md5.hexdigest()

    @commands.Cog.listener()
    async def on_ready(self):
        print('Local NSFW Cog Loaded')

    @commands.command(description = 'Sends generic Hentai.', aliases = ['hentai'])
    async def sauce(self, ctx):
        imgList = os.listdir("/var/www/html/HDD/NSFW/AnimeNSFW/lol/")
        imgString = random.choice(imgList)
        path = "/var/www/html/HDD/NSFW/AnimeNSFW/lol/" + imgString
        await self.client.get_channel(354294536198946817).send(file=discord.File(path))
    @commands.command(description = 'Sends mostly IRL Tights and Stockings.', aliases = ['thights', 'stockings', 'thighhighs', 'socks', 'kneesocks', 'tights'])
    async def thighs(self, ctx):
        imgList = os.listdir("/var/www/html/HDD/NSFW/Thighs/")
        imgString = random.choice(imgList)
        path = "/var/www/html/HDD/NSFW/Thighs/" + imgString
        await self.client.get_channel(354294536198946817).send(file=discord.File(path))
    @commands.command(description = 'Sends mostly IRL Tights and Stockings with some quality control.')
    async def goodthighs(self, ctx):
        imgList = os.listdir("/var/www/html/HDD/NSFW/Thighs/Good/")
        imgString = random.choice(imgList)
        path = "/var/www/html/HDD/NSFW/Thighs/Good/" + imgString
        await self.client.get_channel(354294536198946817).send(file=discord.File(path))

    def reacted_check(self, payload):
        if payload.user_id == self.client.user.id:
            return False
        if payload.message_id != self.msg.id:
            return False

        to_check = str(payload.emoji)
        reaction_emojis = [('⬅️', self.prev_page), ('➡️', self.next_page)]
        for (emoji, func) in reaction_emojis:
            if to_check == emoji:
                self.match = func
                return True
        return False

    async def send_post(self, ctx, folder_name, post_number, full_dir, edit=False):
        url = f'5124.mywire.org/NSFW.html/{folder_name}/'
        if edit is False:
            x_encoded = urllib.parse.quote(full_dir[post_number])
            url_encoded = urllib.parse.quote(url)
            url_encoded = 'https://' + url_encoded

            self.embed = embed = discord.Embed(colour=0xee22ee, title=folder_name.title(), url=url_encoded)
            embed.description = f'{post_number}/{len(full_dir)}'
            embed.set_image(url=url_encoded + x_encoded)

            self.msg = await ctx.send("Loading images, Please wait...")

            await self.msg.edit(embed=embed, content="")
            await self.msg.add_reaction('⬅️')
            await self.msg.add_reaction('➡️')
        else:
            x_encoded = urllib.parse.quote(full_dir[self.num])
            url_encoded = urllib.parse.quote(url)
            url_encoded = 'https://' + url_encoded

            self.embed = embed = discord.Embed(colour=0xee22ee, title=folder_name.title(), url=url_encoded)
            embed.description = f'{self.num}/{len(full_dir)}'
            embed.set_image(url=url_encoded + x_encoded)

            await self.msg.edit(embed=embed)

    async def send_help(self, ctx):
        paginator = commands.Paginator(prefix='```md')
        paginator.add_line('+ The arrows will stop working after 2 minutes of inactivity.\n\
+ Run the command with any of the next id\'s:\n')
        max_perm_length = max(len(p) for (i,p) in self.folders) + 1

        for (num, folder) in self.folders:
            paginator.add_line(f'- {folder.title(): <{max_perm_length}} --> {num}')

        for page in paginator.pages:
            await ctx.send(page)


    @commands.command(aliases = ['porn'])
    async def lnsfw(self, ctx, _id:int=0):
        if ctx.channel is not None:
            try:
                if not ctx.channel.is_nsfw():
                    return await ctx.send('You are only allowed to use this command in NSFW Channels or DM\'s')
            except AttributeError:
                pass

        if _id == 0:
            return await self.send_help(ctx)

        self.client.reload_extension('cogs.localnsfw')

        for (num, folder) in self.folders:
            if num == _id:
                if _id == 727:
                    x = random.choice(self.folders)
                    folder_name = x[1]
                else:
                    folder_name = folder

        base_dir = '/mnt/SMB/NSFW/'

        full_dir = os.listdir(base_dir + folder_name)
        self.num = post_number = random.randint(0, len(full_dir))

        await self.send_post(ctx, folder_name, post_number, full_dir, edit=False)
        self.ctx = ctx

        for _ in range(0, len(full_dir)):
            done, pending = await asyncio.wait([
                self.client.wait_for('raw_reaction_add', check=self.reacted_check),
                self.client.wait_for('raw_reaction_remove', check=self.reacted_check)
                ], return_when=asyncio.FIRST_COMPLETED, timeout=120)
            if not done:
                for future in pending:
                    future.cancel()
                try:
                    await self.msg.clear_reactions()
                except:
                    pass
                finally:
                    break

            try:
                payload = done.pop().result()
            except:
                for future in pending:
                    future.cancel()
                try:
                    await self.msg.clear_reactions()
                except:
                    pass
                finally:
                    break
            for future in pending:
                future.cancel()

            try:
                await self.msg.remove_reaction(payload.emoji, discord.Object(id=payload.user_id))
            except:
                pass 

            await self.match()
            await self.send_post(ctx, folder_name, post_number, full_dir, edit=True)

    @commands.Cog.listener()
    async def on_message(self, message):
        if message.author == self.client.user:
            return
        elif message.author.id == 350432098315927574:
            return
        else:
            if message.channel.id == 354294536198946817:
                for item in message.attachments:
                    req = urllib.request.Request(item.url, headers={'User-Agent' : "Magic Browser"})
                    con = urllib.request.urlopen(req)
                    heximage = con.read()

                    filename = "./localNSFW/pic"
                    with open(filename, 'wb') as image:
                        image.write(heximage)

                    if os.path.isfile(f'./localNSFW/{await self.md5(filename)}') == True:
                        os.remove(filename)
                        #await message.add_reaction('👁‍🗨')
                    else:
                        os.rename(filename, f'./localNSFW/{await self.md5(filename)}')

def setup(client):
    client.add_cog(LocalNSFW(client))
