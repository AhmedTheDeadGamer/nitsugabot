"""
Calculates the PP values of a given score.
Takes: Accuracy, Combo, Max Combo, Mods, 300/100/50/miss and Star Difficulty
"""
import math

def calculation(aim_stars, speed_stars, combo, max_combo, map_od, map_ar, total_hits, _miss, _300, _100, _50, num_hit_circles, acc, mods_list):
    multiplier = 1.12

    if "NF" in mods_list:
        multiplier *= 0.90
    if "SO" in mods_list:
        multiplier *= 0.95

    aim_value = calculate_aim_value(aim_stars, total_hits, _miss, combo, max_combo, map_ar, map_od, acc, mods_list)
    speed_value = calculate_speed_value(speed_stars, _miss, total_hits, combo, max_combo, map_ar, map_od, acc, mods_list)
    acc_value = calculate_acc_value(num_hit_circles, _300, _100, _50, total_hits, map_od, mods_list)

    print(f'aim {aim_value}')
    print(f'speed {speed_value}')
    print(f'acc {acc_value}')

    total_value = pow(
        pow(aim_value, 1.1) +
        pow(speed_value, 1.1) +
        pow(acc_value, 1.1), 1 / 1.1
    ) * multiplier

    return total_value

def calculate_speed_value(speed_stars, _miss, total_hits, combo, max_combo, map_ar, map_od, acc, mods_list=[]):
    """
    Calculates the speed PP Value
    """
    speed_value = pow(5 * max(1, speed_stars / 0.0675) - 4, 3) / 100000

    speed_value *= 0.95 + 0.4 * min(1, total_hits / 2000) + (total_hits > 2000 if math.log10(total_hits / 2000) * 0.5 else 0)

    speed_value *= pow(0.97, _miss)

    if max_combo > 0:
        speed_value *= min(pow(combo, 0.8) / pow(max_combo, 0.8), 1)

    ar_factor = 1
    if map_ar > 10.33:
        ar_factor += 0.3 * (map_ar - 10.33)

    speed_value *= ar_factor

    if "HD" in mods_list:
        speed_value *= 1 + 0.04 * (12 - map_ar)

    speed_value *= 0.02 + acc
    speed_value *= 0.96 + pow(map_od, 2) / 1600

    return speed_value


def calculate_aim_value(aim_stars, total_hits, _miss, combo, max_combo, map_ar, map_od, acc, mods_list=[]):
    """
    Calculates the aim PP Value
    """
    raw_aim = aim_stars

    if "TD" in mods_list:
        raw_aim = pow(raw_aim, 0.8)

    aim_value = pow(5 * max(1, raw_aim / 0.0675) - 4, 3) / 100000

    length_bonus = 0.95 + 0.4 * min(1, total_hits / 2000) + (total_hits > 2000 if math.log10(total_hits / 2000) * 0.5 else 0)

    aim_value *= length_bonus

    aim_value *= pow(0.97, _miss)

    if max_combo > 0:
        aim_value *= min(pow(combo, 0.8) / pow(max_combo, 0.8), 1)

    ar_factor = 1

    if map_ar > 10.33:
        ar_factor += 0.3 * (ar_factor - 10.33)
    elif map_ar < 8:
        ar_factor += 0.01 * (8 - map_ar)

    aim_value *= ar_factor

    if "HD" in mods_list:
        aim_value *= 1 + 0.04 * (12 - map_ar)
    if "FL" in mods_list:
        aim_value *= 1 + 0.35 * min(1, total_hits / 200) + (total_hits > 200 if 0.3 * min(1, (total_hits - 200) / 300) + (total_hits > 500 if (total_hits - 500) / 1200 else 0) else 0)

    aim_value *= 0.5 + acc / 2
    aim_value *= 0.98 + pow(map_od, 2) / 2500

    return aim_value

def calculate_acc_value(num_hit_circles, _300, _100, _50, total_hits, map_od, mods_list=[]):
    """
    Calculates the accuracy PP Value
    """
    if num_hit_circles > 0:
        better_acc_percentage = ((_300 - (total_hits - num_hit_circles)) * 6 + _100 * 2 + _50) / (num_hit_circles * 6)
    else:
        better_acc_percentage = 0

    if better_acc_percentage < 0:
        better_acc_percentage = 0

    accuracy_value = pow(1.52163, map_od) * pow(better_acc_percentage, 24) * 2.83
    accuracy_value *= min(1.15, pow(num_hit_circles / 1000, 0.3))

    if 'HD' in mods_list:
        accuracy_value *= 1.08
    if 'FL' in mods_list:
        accuracy_value *= 1.02

    return accuracy_value

#print(calculation(1.3, 3.3, 2000, 2000, 8, 9.7, 1500, 15, 1400, 85, 0, 1000, 99.32, ["NF", "HD"]))
#print(calculation(4.4, 3.3, 2000, 2000, 8, 9.7, 1500, 15, 1400, 85, 0, 1000, 99.32, ['HD']))

#print(calculate_aim_value(4.4, 1500, 0, 2000, 2000, 9.7, 8, 99.32))
#print(calculate_speed_value(3.3, 0, 1500, 2000, 9.7, 8, 99.32))
#print(calculate_acc_value(1000, 1400, 85, 0, 1500, 8))

#print(calculation(2.54215, 2.83749, 863, 865, 8, 9, 621, 0, 556, 63, 2, 456, 92.97, [])) # 167

print(calculation(4.82601, 2.15843, 909, 909, 8, 8, 641, 0, 640, 1, 0, 404, 99.9, ["HD", "DT"]))
