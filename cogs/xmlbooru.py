import random
import os
import re
import json
import traceback
import sys
import discord
from discord.ext import commands
import aiohttp
import aiofiles
import xmltodict
from collections import OrderedDict
import cogs.debug
from utils.alpha_booru import main as alpha_booru

class XMLBooru(commands.Cog):
    def __init__(self, client):
        self.client = client

        if self.client.xml_cog is True:
            with open('boorus.json', 'r') as booru_list:
                booru_dict = json.load(booru_list)

                for item in booru_dict['boorus'].items():
                    try:
                        booru_command = f"\
import cogs.xmlbooru\n\n\
@client.command(aliases = {item[1]['aliases']})\n\
async def {item[0]}(ctx, flag=None, *args):\n\
    client.booru = '{item[0]}'\n\
    await cogs.xmlbooru.XMLBooru(client).flag_func(ctx, flag, args)"
                        self.client.loop.create_task(cogs.debug.Debug(client).ctxless_eval(booru_command))
                    except Exception as error:
                        #traceback.print_exception(type(error), error, error.__traceback__, file=sys.stderr)
                        pass
        self.client.xml_cog = False

    @commands.Cog.listener()
    async def on_ready(self):
        print('XMLBooru Cog Loaded')

    async def url_format_1(self, link, tags, page):
        return f"https://{link}/index.php?page=dapi&s=post&q=index&tags={tags}&pid={page}&limit=20"
    async def url_format_2(self, link, tags, page):
        return f"https://{link}/post/index.xml?tags={tags}&page={page}&limit=20"

    async def api_call(self, tags, page=0):
        with open('boorus.json', 'r') as booru_list:
            booru_dict = json.load(booru_list)

            for item in booru_dict['boorus'].items():
                if item[0] == self.client.booru:
                    link = item[1]["url"]
                    if item[1]["type"] == 1:
                        self.id = 1
                        url = await self.url_format_1(link, tags, page)
                        return await self.chosen_api(url)
                    elif item[1]["type"] == 2:
                        self.id = 2
                        url = await self.url_format_2(link, tags, page)
                        return await self.chosen_api(url)
                    elif item[1]["type"] == 3:
                        self.id = 3
                        data = await alpha_booru(f"https://{link}/", tags=tags, page=page)
                        return data['posts']

    async def chosen_api(self, url):
        try:
            async with aiohttp.request("GET", url, headers={'User-Agent' : "Magic Browser"}) as url:
                data = await url.text()

                dict_data = xmltodict.parse(data)
                try:
                    return dict_data['posts']['post']
                except KeyError:
                    return 0
        except:
            url = re.sub('https', 'http', url)
            async with aiohttp.request("GET", url, headers={'User-Agent' : "Magic Browser"}) as url:
                data = await url.text()

                dict_data = xmltodict.parse(data)
                try:
                    return dict_data['posts']['post']
                except KeyError:
                    return 0

    async def check_nsfw(self, ctx):
        self.client.is_nsfw = ctx.guild is None or ctx.channel.is_nsfw()

    async def send_post(self, ctx, preset_data):
        rating = preset_data[0]
        score = preset_data[1]
        source = preset_data[2]
        full_size = preset_data[3]
        sample = preset_data[4]

        filename = f'{sample.split("/")[-1]}'
        filename = filename.split('?', 1)[0]

        if "Konachan" in filename:
            filename = filename[-12:]

        async with aiohttp.request("GET", full_size, headers={'User-Agent' : "Magic Browser"}) as url:
            if url.status == 200:
                file_img = await aiofiles.open(filename, mode='wb')
                await file_img.write(await url.read())
                await file_img.close()


        if rating == 's':
            rating = 'Safe'
        elif rating == 'q':
            rating = 'Questionable'
        elif rating == 'e':
            rating = 'Explicit'

        embed = discord.Embed(colour=discord.Colour(0x222222))

        #embed.set_image(url=sample)
        image = discord.File(filename)
        embed.set_image(url=f'attachment://{filename}')
        embed.set_author(name="Full size image. (Click here)", url=full_size)
        embed.add_field(name="Rating", value=rating, inline=True)
        embed.add_field(name="Score", value=score, inline=True)

        if source != '':
            if 'http' in source:
                embed.add_field(name="Source", value=f"[Here]({source})", inline=True)
            else:
                embed.add_field(name="Source", value=source, inline=True)
        try:
            self.client.image = await ctx.send(embed=embed, file=image)
        except Exception:
            await ctx.send(f'File was too big to send, so here\'s the url:\n{full_size}')

        os.remove(filename)
        self.client.source = None


    async def set_data(self, post):
        try:
            preset_data = [post['@rating']]
        except:
            preset_data = [post['rating']]

        try:
            preset_data.append(post['@score'])
        except:
            preset_data.append(post['score'])

        if isinstance(preset_data[1], OrderedDict):
            x = preset_data[1]["#text"]
            preset_data.pop(1)
            preset_data.append(x)

        try:
            preset_data.append(post['@source'])
        except:
            preset_data.append(post['source'])


        try:
            if post['@file_url'].startswith('//'):
                preset_data.append('https:'+post['@file_url'])
            else:
                preset_data.append(post['@file_url'])
            print(preset_data[3])
        except:
            if post['file_url'].startswith('//'):
                preset_data.append('https:'+post['file_url'])
            else:
                preset_data.append(post['file_url'])

        try:
            if post['@sample_url'].startswith('//'):
                preset_data.append('https:'+post['@sample_url'])
            else:
                preset_data.append(post['@sample_url'])
        except:
            if post['sample_url'].startswith('//'):
                preset_data.append('https:'+post['sample_url'])
            else:
                preset_data.append(post['sample_url'])

        try:
            self.client.last_md5 = post['@md5']
        except:
            self.client.last_md5 = post['md5']

        return preset_data

    async def page_loop(self, data, tags):
        page = 0

        try:
            while len(data) == 20:
                page += 1
                data = await self.api_call(tags, page)

                if page == 4:
                    break
        except TypeError as error:
            print(error)

        self.client.datalen = len(data)

        if page < 2:
            pagef = 0
        else:
            pagef = 2
        page += 1
        page = random.choice(range(pagef, page))
        return page

    async def pick_post(self, tags, page):
        data = await self.api_call(tags, page)
        post_num = random.choice(range(0, self.client.datalen))
        try:
            post = data[post_num]
        except KeyError:
            post = data
        return post

    async def functions(self, ctx, tags):
        data = await self.api_call(tags)

        if data == 0:
            await ctx.send('One of the tags does not exist')
            return

        page = await self.page_loop(data, tags)
        if page == 727:
            return
        post = await self.pick_post(tags, page)
        preset_data = await self.set_data(post)
        await self.send_post(ctx, preset_data)

    async def flag_func(self, ctx, flag, args):
        await self.check_nsfw(ctx)
        output = ''
        for word in args:
            output += word
            output += '%20'
            if 'rating' in output:
                if self.client.is_nsfw is True:
                    await ctx.send('You are not allowed to use custom ratings outside nsfw channels or dm\'s')
                    return
        output = re.sub("\s", "", (output.lower()))[:-3]

        if flag is None:
            output += '%20rating:safe'
            await self.functions(ctx, output)
        elif flag == '-r':
            if self.client.is_nsfw is True:
                await self.functions(ctx, output)
            else:
                await ctx.send('You are not allowed to use that flag outside nsfw channels or dm\'s')
        elif flag == '-n':
            if self.client.is_nsfw is True:
                choice = random.choice(['explicit', 'questionable'])
                output += f'%20rating:{choice}'
                await self.functions(ctx, output)
            else:
                await ctx.send('You are not allowed to use that flag outside nsfw channels or dm\'s')
        elif flag == '-x':
            if self.client.is_nsfw is True:
                output += '%20rating:explicit'
                await self.functions(ctx, output)
            else:
                await ctx.send('You are not allowed to use that flag outside nsfw channels or dm\'s')
        elif flag == '-u':
            output = f'rating:safe%20user:{output}'
            await self.functions(ctx, output)
        else:
            if 'rating' in flag:
                if self.client.is_nsfw is True:
                    await ctx.send('You are not allowed to use custom ratings outside nsfw channels or dm\'s')
                    return
                output += f'%20{flag}'
            else:
                if 'rating' in output:
                    output += f'%20{flag}'
                else:
                    output += f'%20rating:safe%20{flag}'
            await self.functions(ctx, output)

    @commands.command()
    async def touhou(self, ctx, *, args=None):
        if args is not None:
            await ctx.invoke(self.client.get_command('safebooru'), 'touhou', args)
        else:
            await ctx.invoke(self.client.get_command('safebooru'), 'touhou')

def setup(client):
    client.add_cog(XMLBooru(client))
