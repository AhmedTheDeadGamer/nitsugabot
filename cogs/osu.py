import json
import datetime
import time
import aiohttp
import discord
from discord.ext import commands, menus


class OsuTop(menus.Menu):
    def __init__(self, username, mode, client):
        super().__init__(timeout=120.0, clear_reactions_after=True)
        self.username = username
        self.mode = mode
        self.client = client
        self.top = 0

    async def get_data(self, pos):
        self.pos = pos
        return self.top_data[pos]

    async def get_extra_data(self, data):
        extra_data = await osu_game.beatmap_api_call(osu_game(self.client), data['beatmap_id'], self.mode)

        return extra_data

    async def build_embed(self, data, extra_data):
        formatted_date = datetime.datetime.strptime(data['date'], '%Y-%m-%d %H:%M:%S')
        unixtime = int(time.mktime(formatted_date.timetuple()))

        embed = discord.Embed(title=f"{extra_data[0]['artist']} - {extra_data[0]['title']}", colour=discord.Colour(0x5bc1), url=f"https://osu.ppy.sh/b/{data['beatmap_id']}", description=f"[{extra_data[0]['version']}] by [{extra_data[0]['creator']}](https://osu.ppy.sh/u/{extra_data[0]['creator_id']}) | N.{self.pos}", timestamp=datetime.datetime.utcfromtimestamp(unixtime))

        embed.set_thumbnail(url=f"https://b.ppy.sh/thumb/{extra_data[0]['beatmapset_id']}l.jpg")
        embed.set_author(name=self.username, url=f"https://osu.ppy.sh/u/{data['user_id']}", icon_url=f"https://a.ppy.sh/{data['user_id']}")
        embed.set_footer(text=f"{data['pp']}pp")

        embed.add_field(name="Score", value=f"{int(data['score']):,}\n{data['maxcombo']}/{extra_data[0]['max_combo']}\n{await osu_game.get_mods(osu_game(self.client), int(data['enabled_mods']))}", inline=True)
        embed.add_field(name="Accuracy", value=f"**{data['rank']}** = {await osu_game.acc_math(osu_game, int(data['count300']), int(data['count100']), int(data['count50']), int(data['countmiss']))}%\n{data['count300']} | {data['countgeki']}\n{data['count100']} | {data['countkatu']}\n{data['count50']} | x{data['countmiss']}", inline=True)
        embed.add_field(name="Map Information", value=f"CS: {extra_data[0]['diff_size']} | OD: {extra_data[0]['diff_overall']} | AR: {extra_data[0]['diff_approach']} | HP: {extra_data[0]['diff_drain']}\nFav: {extra_data[0]['favourite_count']} | Rating: {float(extra_data[0]['rating']):.3}\nSR: {float(extra_data[0]['difficultyrating']):.3}* == {float(extra_data[0]['diff_aim']):.3} Aim + {float(extra_data[0]['diff_speed']):.3} Speed\nFrom {extra_data[0]['last_update']}")
        return embed

    async def send_initial_message(self, ctx, channel):
        self.top_data = await osu_game.user_top_api_call(osu_game(self.client), self.username, self.mode)
        data = await self.get_data(self.top)
        extra_data = await self.get_extra_data(data)
        embed = await self.build_embed(data, extra_data)
        return await channel.send(embed=embed)

    @menus.button('⬅️')
    async def on_left(self, payload):
        self.top -= 1
        data = await self.get_data(self.top)
        extra_data = await self.get_extra_data(data)
        embed = await self.build_embed(data, extra_data)
        await self.message.edit(embed=embed)

    @menus.button('➡️')
    async def on_right(self, payload):
        self.top += 1
        data = await self.get_data(self.top)
        extra_data = await self.get_extra_data(data)
        embed = await self.build_embed(data, extra_data)
        await self.message.edit(embed=embed)

    @menus.button('🔄')
    async def on_reload(self, payload):
        self.top_data = await osu_game.user_top_api_call(osu_game(self.client), self.username, self.mode)
        self.top = 0
        data = await self.get_data(self.top)
        extra_data = await self.get_extra_data(data)
        embed = await self.build_embed(data, extra_data)
        await self.message.edit(embed=embed)

    @menus.button('🛑')
    async def on_stop(self, payload):
        self.stop()



class osu_game(commands.Cog):
    def __init__(self, client):
        self.client = client

    @commands.Cog.listener()
    async def on_ready(self):
        print('osu_game Cog Loaded')

    async def pacman(self, value):
        near = int(value)
        tm = int(value / 50.0)
        x = ""

        for _ in range(tm):
            x += "· "
        x += "C "
        for _ in range(tm, 20):
            x += "o "

        return x

    async def get_user(self, user_id : str):
        with open("osu.json", "r") as osu_players:
            return json.load(osu_players).get(user_id)

    async def reverse_get_user(self, username):
        with open("osu.json", "r") as osu_players:
            data = json.load(osu_players)
        for user, settings in data.items():
            if settings['username'] == username:
                return settings
        return None

    async def acc_math(self, _300, _100, _50, _miss):
        x = _300 + _100 + _50 + _miss

        pcount50 = _50/x*(100/6)
        pcount100 = _100/x*(100/3)
        pcount300 = _300/x*100

        acc = pcount50+pcount100+pcount300
        acc = round(acc, 2)

        return float(acc)

    async def get_mods(self, mods):
        MODS = ["NF", "EZ", "TD", "HD", "HR", "SD", "DT", "RX", "HT", "NC", "FL", "AT", "SO", "AP", "PF", "4K", "5K", "6K", "7K", "8K", "FI", "RD", "CN", "TP", "9K", "CO", "1K", "3K", "2K", "V2"] #J To remove newline VIM
        #mod_list = lambda mods: [MODS[i] for i in range(len(MODS)) if mods & 1 << i] # list
        mod_str = lambda mods: " ".join([MODS[i] for i in range(len(MODS)) if mods & 1 << i]) #string

        return mod_str(mods)

    async def api_call(self, url):
        async with aiohttp.request("GET", url, headers={'User-Agent' : "Magic Browser"}) as url:
            if url.status == 200:
                data = await url.json()
            else:
                return None
        return data

    async def recent_api_call(self, user, mode=0):
        url = f'https://osu.ppy.sh/api/get_user_recent?m=0&u={user}&k={self.client.osu_token}&m={mode}&limit=25'
        return await self.api_call(url)

    async def beatmap_api_call(self, beatmap_id, mods=0):
        url = f'https://osu.ppy.sh/api/get_beatmaps?b={beatmap_id}&k={self.client.osu_token}&mods={mods}'
        return await self.api_call(url)

    async def user_api_call(self, user, mode=0):
        url = f'https://osu.ppy.sh/api/get_user?u={user}&k={self.client.osu_token}&m={mode}'
        return await self.api_call(url)

    async def user_top_api_call(self, user, mode=0):
        url = f'https://osu.ppy.sh/api/get_user_best?u={user}&k={self.client.osu_token}&m={mode}&limit=100'
        return await self.api_call(url)

    async def score_api_call(self, user, map_id, mode=0):
        url = f'https://osu.ppy.sh/api/get_scores?u={user}&b={map_id}&k={self.client.osu_token}&m={mode}'
        return await self.api_call(url)


    async def check_specified(self, ctx, args):
        if args != ():
            if 'taiko' in args:
                if len(args) > 1:
                    for pos, i in enumerate(args):
                        if i != 'taiko':
                            username = args[pos]
                    mode = 1
                    data = await self.recent_api_call(username, mode)
                else:
                    mode = 1
                    user = await self.get_user(str(ctx.author.id))
                    if user is None:
                        username = ctx.author.display_name
                        data = await self.recent_api_call(username, mode)
                    else:
                        username = user['username']
                        data = await self.recent_api_call(username, mode)

            elif 'ctb' in args:
                if len(args) > 1:
                    for pos, i in enumerate(args):
                        if i != 'ctb':
                            username = args[pos]
                    mode = 2
                    data = await self.recent_api_call(username, mode)
                else:
                    mode = 2
                    user = await self.get_user(str(ctx.author.id))
                    if user is None:
                        username = ctx.author.display_name
                        data = await self.recent_api_call(username, mode)
                    else:
                        username = user['username']
                        data = await self.recent_api_call(username, mode)

            elif 'mania' in args:
                if len(args) > 1:
                    for pos, i in enumerate(args):
                        if i != 'mania':
                            username = args[pos]
                    mode = 3
                    data = await self.recent_api_call(username, mode)
                else:
                    mode = 3
                    user = await self.get_user(str(ctx.author.id))
                    if user is None:
                        username = ctx.author.display_name
                        data = await self.recent_api_call(username, mode)
                    else:
                        username = user['username']
                        data = await self.recent_api_call(username, mode)

            elif 'std' in args or 'standard' in args:
                if len(args) > 1:
                    for pos, i in enumerate(args):
                        if i != 'std' or i != 'standard':
                            username = args[pos]
                    mode = 0
                    data = await self.recent_api_call(username, mode)
                else:
                    mode = 0
                    user = await self.get_user(str(ctx.author.id))
                    if user is None:
                        username = ctx.author.display_name
                        data = await self.recent_api_call(username, mode)
                    else:
                        username = user['username']
                        data = await self.recent_api_call(username, mode)

            else:
                username = args[0]
                mode = 0
                data = await self.recent_api_call(username, mode)

        else:
            user = await self.get_user(str(ctx.author.id))
            if user is None:
                await ctx.send('Consider configuring your osu! profile to the bot with `osuc`', delete_after=10)
                username = ctx.author.display_name
                mode = 0
                data = await self.recent_api_call(username, mode)
            else:
                username = user['username']
                mode = user['mode']
                data = await self.recent_api_call(username, mode)

        if data is None or data == []:
            return username
        else:
            data_list = [data, mode]
            return data_list





    async def check_mods(self, mods):
        if 'DT' in mods or 'NC' in mods:
            if 'HR' in mods:
                mod_id = 80
            elif 'EZ' in mods:
                mod_id = 68
            else:
                mod_id = 64
        elif 'HT' in mods:
            if 'HR' in mods:
                mod_id = 272
            elif 'EZ' in mods:
                mod_id = 258
            else:
                mod_id = 256
        elif 'HR' in mods:
            mod_id = 16
        elif 'EZ' in mods:
            mod_id = 2
        else:
            mod_id = 0

        return mod_id
    async def progress_math(self, count_normal, count_slider, count_spinner, _300, _100, _50, _miss):
        all_the_things = count_normal + count_slider + count_spinner
        everything = _300 + _100 + _50 + _miss
        progress = everything / all_the_things * 100
        return round(progress, 2)

    async def check_retry(self, data):
        x = data[0]['beatmap_id']
        y = x
        f = 1
        while y == x:
            if f == len(data):
                return f
            if data[f]['beatmap_id'] != x:
                return f
            if f < len(data):
                f += 1
            else:
                return '25+'

    async def short_embed_builder(self, artist, title, difficulty_name, mapper, beatmapurl, score, combo, max_combo, acc, _300, _100, _50, _miss, tries, progress, unixtime, beatmap_s_id, username, user_id, difficulty, mods, rating, pp=True):
        if rating == 'F':
            rating_url = 'https://5124.mywire.org/HDD/Downloads/BoneF.png'
        else:
            rating_url = f"https://s.ppy.sh/images/{rating.upper()}.png"
        embed = discord.Embed(title=f"{artist} - {title}  [**{difficulty_name}**]\nby {mapper}", colour=discord.Colour(0xdf58f4), url=beatmapurl, description=f"**{score}** ┇ **x{combo} / {max_combo}**\n**{acc}%** ┇ {_300} - {_100} - {_50} - {_miss}\nTry #{tries} ━ Progress: {progress}%", timestamp=datetime.datetime.utcfromtimestamp(unixtime))

        embed.set_thumbnail(url=f"https://b.ppy.sh/thumb/{beatmap_s_id}l.jpg")
        embed.set_author(name=username, url=f"https://osu.ppy.sh/u/{user_id}", icon_url=f"https://a.ppy.sh/{user_id}")
        if pp is True:
            embed.set_footer(text=f"PP | NEW_PP | {round(float(difficulty), 2)}* | {mods}", icon_url=rating_url)
        else:
            embed.set_footer(text=f"{round(float(difficulty), 2)}* | {mods}", icon_url=rating_url)

        return embed

    async def long_embed_builder(self):
        return

    async def user_embed_builder(self, yes_pp, user_id, _300, _100, _50, user_acc, plays, total_score, ranked_score, played_seconds, rank, local_rank, user_level, unixtime, user_name, user_country, user_pp, _SSH, _SS, _SH, _S, artist, title, difficulty_name, mapper, beatmap_id, score, combo, max_combo, mods, ranking, acc, m_300, m_100, m_50, m_miss, map_pp, play_date, cs, od, ar, hp, favorites, rating, stars, stars_aim, stars_speed, map_date):
        if int(user_id) < 16777215:
            _id = int(hex(int(user_id)), base=16)
        else: _if = 0xF748BC

        if yes_pp != 0:
            embed = discord.Embed(colour=discord.Colour(_id), description=f"**{_300:,} -- {_100:,} -- {_50:,}** == 300/100/50\n**{user_acc:.2f}% -- {plays:,}** Plays\nTotal = **{total_score:,}** | Ranked = **{ranked_score:,}**\nGlobal: #**{rank:,}** | Country: #**{local_rank:,}**\nPlayed **{played_seconds:,}** seconds or: **{datetime.timedelta(seconds=played_seconds)}**\n**L{int(user_level)}** > Next level: `{await self.pacman(int(str(user_level-int(user_level))[1:].strip('.')[:3]))}`", timestamp=datetime.datetime.utcfromtimestamp(unixtime))

            embed.set_image(url=f"https://a.ppy.sh/{user_id}")
            embed.set_author(name=f"{user_name}", url=f"https://osu.ppy.sh/u/{user_id}", icon_url=f"https://raw.githubusercontent.com/stevenrskelton/flag-icon/master/png/75/country-squared/{user_country.lower()}.png")
            embed.set_footer(text=f"PP = {user_pp} - SSH:{_SSH} | SS:{_SS} | SH:{_SH} | S:{_S}")

            embed.add_field(name="Top Play:", value=f"[**{artist} - {title}** [*{difficulty_name}*] by {mapper}](https://osu.ppy.sh/b/{beatmap_id})\n**{score:,}** -- {combo}/{max_combo} + **{mods}**\n**{ranking}** = {acc:.2f}% = {m_300}/{m_100}/{m_50}/{m_miss}\nPP = {map_pp}\n{play_date}\n\nCS: {cs} | OD: {od} | AR: {ar} | HP: {hp}\nFav: {favorites} | Rating: {rating:.2f}\nSR: **{stars:.2f}*** == **{stars_aim:.2f}** Aim + **{stars_speed:.2f}** Speed\nFrom {map_date}")
        else:
            embed = discord.Embed(colour=discord.Colour(_id), description=f"**{_300:,} -- {_100:,} -- {_50:,}** == 300/100/50\n**{user_acc:.2f}% -- {plays:,}** Plays\nTotal = **{total_score:,}** | Ranked = **{ranked_score:,}**\nPlayed **{played_seconds:,}** seconds or: **{datetime.timedelta(seconds=played_seconds)}**\n**L{int(user_level)}** > Next level: `{await self.pacman(int(str(user_level-int(user_level))[1:].strip('.')[:3]))}`", timestamp=datetime.datetime.utcfromtimestamp(unixtime))

            embed.set_image(url=f"https://a.ppy.sh/{user_id}")
            embed.set_author(name=f"{user_name}", url=f"https://osu.ppy.sh/u/{user_id}", icon_url=f"https://raw.githubusercontent.com/stevenrskelton/flag-icon/master/png/75/country-squared/{user_country.lower()}.png")
            embed.set_footer(text=f"SSH:{_SSH} | SS:{_SS} | SH:{_SH} | S:{_S}")

        return embed


    @commands.command(aliases = ['rs', 'rc'])
    async def recent(self, ctx, *args):
        data_list = await self.check_specified(ctx, args)
        data_full = data_list[0]
        data = data_list[0][0]
        mode = data_list[1]
        if isinstance(data_list, str): 
            return await ctx.send(f'User **{data_list}** doesn\'t exist or has not played in the last 24 hours\nIf the username is wrong, set one with `n!osuc` or specify it inside the command.')

        _300 = int(data['count300'])
        _100 = int(data['count100'])
        _50 = int(data['count50'])
        _miss = int(data['countmiss'])
        acc = await self.acc_math(_300, _100, _50, _miss)

        score = data["score"]
        score = "{:,}".format(int(score))

        if data['perfect'] != '0':
            FC = True

        beatmap_id = data['beatmap_id']
        user = data['user_id']
        date = data['date']
        rank = data['rank']
        combo = data['maxcombo']
        mods = data['enabled_mods'] #mods in decimal string

        formatted_date = datetime.datetime.strptime(date, '%Y-%m-%d %H:%M:%S')
        unixtime = int(time.mktime(formatted_date.timetuple()))

        mods = await self.get_mods(int(mods))

        beatmapurl = f'https://osu.ppy.sh/b/{beatmap_id}'

        mod_id = await self.check_mods(mods)
        beatmap_data = await self.beatmap_api_call(beatmap_id, mod_id)
        beatmap_data = beatmap_data[0]

        max_combo = beatmap_data['max_combo']
        map_artist = beatmap_data['artist']
        map_title = beatmap_data['title']
        map_difficulty = beatmap_data['version']
        map_stars = beatmap_data['difficultyrating']
        mapper = beatmap_data['creator']
        map_s_id = beatmap_data['beatmapset_id']

        count_normal = int(beatmap_data['count_normal'])
        count_slider = int(beatmap_data['count_slider'])
        count_spinner = int(beatmap_data['count_spinner'])

        progress = await self.progress_math(count_normal, count_slider, count_spinner, _300, _100, _50, _miss)

        user_data = await self.user_api_call(data["user_id"], mode)
        user_data = user_data[0]


        username = user_data['username']
        user_id = user_data['user_id']

        tries = await self.check_retry(data_full)

        user = await self.get_user(str(ctx.author.id))
        if user is not None:
            if args == ():
                username = user['username']
                if user['pp'] == 0:
                    pp = False
                else:
                    pp = True
            else:
                pp = True
            length = user['recent']
        else:
            pp = True
            length = 'short'
        if length == 'short':
            embed = await self.short_embed_builder(map_artist, map_title, map_difficulty, mapper, beatmapurl, score, combo, max_combo, acc, _300, _100, _50, _miss, tries, progress, unixtime, map_s_id, username, user_id, map_stars, mods, rank, pp)
        else:
            embed = await self.long_embed_builder()

        try:
            self.client.last_recent = await ctx.send(embed=embed, content=f"{user_id} | {beatmap_id}")
        except:
            self.client.last_recent = await ctx.author.send(embed=embed, content=f"{user_id} | {beatmap_id}")

    @commands.command(aliases = ['osuprofile', 'oprofile', 'op', 'osup'])
    async def osu_profile(self, ctx, user=None, yes_pp=None):
        if user is None:
            data = await self.get_user(str(ctx.author.id))

            if data is None:
                username = ctx.author.display_name
                if yes_pp is None: yes_pp = 1
            else:
                username = data['username']
                yes_pp = data['pp']

        else:
            data = await self.reverse_get_user(user)

            if data is None:
                username = user
                if yes_pp is None: yes_pp = 1
            else:
                username = data['username']
                yes_pp = data['pp']

        user = await self.user_api_call(username)
        user = user[0]
        user_top = await self.user_top_api_call(username)
        user_top = user_top[0]

        top_beatmap_id = user_top['beatmap_id']

        beatmap = await self.beatmap_api_call(top_beatmap_id)
        beatmap = beatmap[0]

        date = user['join_date']
        formatted_date = datetime.datetime.strptime(date, '%Y-%m-%d %H:%M:%S')
        unixtime = int(time.mktime(formatted_date.timetuple()))

        user_id = user['user_id']
        _300 = int(user['count300'])
        _100 = int(user['count100'])
        _50 = int(user['count50'])
        user_acc = float(user['accuracy'])
        plays = int(user['playcount'])
        total_score = int(user['total_score'])
        ranked_score = int(user['ranked_score'])
        played_seconds = int(user['total_seconds_played'])
        rank = int(user['pp_rank'])
        local_rank = int(user['pp_country_rank'])
        user_level = float(user['level'])
        user_name = user['username']
        user_country = user['country']
        user_pp = float(user['pp_raw'])
        _SSH = int(user['count_rank_ssh'])
        _SS = int(user['count_rank_ss'])
        _SH = int(user['count_rank_sh'])
        _S = int(user['count_rank_s'])
        artist = beatmap['artist']
        title = beatmap['title']
        difficulty_name = beatmap['version']
        mapper = beatmap['creator']
        beatmap_id = int(beatmap['beatmap_id'])
        score = int(user_top['score'])
        combo = int(user_top['maxcombo'])
        max_combo = int(beatmap['max_combo'])
        mods = await self.get_mods(int(user_top['enabled_mods']))
        if mods == '': mods = 'NM'
        ranking = user_top['rank']
        m_300 = int(user_top['count300'])
        m_100 = int(user_top['count100'])
        m_50 = int(user_top['count50'])
        m_miss = int(user_top['countmiss'])
        acc = await self.acc_math(m_300, m_100, m_50, m_miss)
        map_pp = float(user_top['pp'])
        play_date = user_top['date']
        cs = float(beatmap['diff_size'])
        od = float(beatmap['diff_overall'])
        ar = float(beatmap['diff_approach'])
        hp = float(beatmap['diff_drain'])
        favorites = int(beatmap['favourite_count'])
        rating = float(beatmap['rating'])
        stars = float(beatmap['difficultyrating'])
        stars_aim = float(beatmap['diff_aim'])
        stars_speed = float(beatmap['diff_speed'])
        map_date = beatmap['submit_date']


        embed = await self.user_embed_builder(yes_pp, user_id, _300, _100, _50, user_acc, plays, total_score, ranked_score, played_seconds, rank, local_rank, user_level, unixtime, user_name, user_country, user_pp, _SSH, _SS, _SH, _S, artist, title, difficulty_name, mapper, beatmap_id, score, combo, max_combo, mods, ranking, acc, m_300, m_100, m_50, m_miss, map_pp, play_date, cs, od, ar, hp, favorites, rating, stars, stars_aim, stars_speed, map_date)
        await ctx.send(embed=embed)

    @commands.command(aliases = ['comp', 'c'])
    async def compare(self, ctx, map_id=None):
        if map_id is None:
            return await ctx.send("Your probably want to use `n!score {map_B_id}`\nIf you want to get the map id from the recent, it's the __second__ value specified on the recent message content.")
        else:
            await ctx.invoke(self.client.get_command('score'), map_id)

    @commands.command()
    async def score(self, ctx, map_id=None):
        if map_id is None:
            return await ctx.send("Please, specify the id of the map you desire to get your score from.\nTIP: The map id is specified as the __second__ value in the recent command.")

        data = await self.get_user(str(ctx.author.id))

        if data is None:
            await ctx.send('Consider configuring your osu! profile to the bot with `osuc`', delete_after=10)
            user_name = ctx.author.display_name
            show_pp = 0
            mode = 0
        else:
            user_name = data['username']
            show_pp = data['pp']
            mode = data['mode']

        score_data = await self.score_api_call(user_name, map_id, mode)
        if score_data == []:
            return await ctx.send(f"{user_name} does not have a score on that map.")
        score_data = score_data[0]
        beatmap_data = await self.beatmap_api_call(map_id)
        beatmap_data = beatmap_data[0]

        score = score_data['score']
        combo = score_data['maxcombo']
        _50 = int(score_data['count50'])
        _100 = int(score_data['count100'])
        _300 = int(score_data['count300'])
        _miss = int(score_data['countmiss'])
        user_id = score_data['user_id']
        date = score_data['date']
        pp = score_data['pp']
        rating = score_data['rank']
        max_combo = beatmap_data['max_combo']
        map_s_id = beatmap_data['beatmapset_id']

        mods = await self.get_mods(int(score_data['enabled_mods']))

        mapper = beatmap_data['creator']
        artist = beatmap_data['artist']
        title = beatmap_data['title']
        diff = beatmap_data['version']

        formatted_date = datetime.datetime.strptime(date, '%Y-%m-%d %H:%M:%S')
        unixtime = int(time.mktime(formatted_date.timetuple()))

        acc = await self.acc_math(_300, _100, _50, _miss)

        if rating == 'F':
            rating_url = 'https://5124.mywire.org/HDD/Downloads/BoneF.png'
        else:
            rating_url = f"https://s.ppy.sh/images/{rating.upper()}.png"


        embed = discord.Embed(title=f"{artist} - {title} [**{diff}**]", colour=discord.Colour(int(user_id)), url=f"https://osu.ppy.sh/b/{map_id}", description=f"by {mapper} | {mods}\nScore: {score}", timestamp=datetime.datetime.utcfromtimestamp(unixtime))

        embed.set_thumbnail(url=f"https://b.ppy.sh/thumb/{map_s_id}l.jpg")
        embed.set_author(name=user_name, url=f"https://osu.ppy.sh/u/{user_id}", icon_url=f"https://a.ppy.sh/{user_id}")
        embed.add_field(name="Acc", value=f"{acc:.2f}% - {_300}/{_100}/{_50}/{_miss}")
        embed.add_field(name="Combo", value=f"x{combo}/{max_combo}")

        if not show_pp:
            embed.set_footer(text="Played", icon_url=rating_url)
        else:
            embed.set_footer(text=f"PP = {pp} | Played", icon_url=rating_url)

        await ctx.send(embed=embed)

    @commands.command(aliases = ['osuname', 'username', 'configosu', 'osuconfig', 'osuc'])
    async def osu_name(self, ctx, username=None, recent='short', pp:int=1, mode:int=0):
        if username is None:
            return await ctx.send('```md\nPlease specify: osu! username or ID, Recent type, Show pp?, Mode ID\n\nExample:\n> n!osuc nitsuga5124 short 0 1\n# This sets your username to `nitsuga5124`, the embed type to `short`, that you don\'t want to see the pp of the play, and that you want it to show your `Taiko` scores by default\nMode id\'s: STD: 0 | Taiko: 1 | CTB: 2 | Mania: 3\n\n- Specifying just the username also works, it will default to short, show pp and std mode.```')

        new_dict = {"username" : username, "mode" : mode, "recent" : recent, "pp" : pp}
        with open('osu.json', 'r') as fn:
            data_json = json.load(fn)
        data_json[str(ctx.author.id)] = new_dict
        with open('osu.json', 'w') as fn:
            json.dump(data_json, fn, indent=4, sort_keys=True)
        await ctx.send(f'Successfully changed your configuration!\nCurrent configuration:\n```Username: {username}\nRecent type: {recent}\nShow pp? {pp}\nDefault gamemode: {mode}```')

    @commands.command(aliases = ['osutop', 'osut', 'top'])
    async def osu_top(self, ctx, user=None):
        if user is not None:
            data = await self.reverse_get_user(user)
        else:
            data = await self.get_user(str(ctx.author.id))

        if data is None:
            username = user
            mode = 0
        elif not data['pp']:
            return await ctx.send(f'The user **{data["username"]}** does not want anything to do with pp.')
        else:
            username = data['username']
            mode = data['mode']

        m = OsuTop(username, mode, self.client)
        await m.start(ctx)


def setup(client):
    client.add_cog(osu_game(client))
