import discord
from discord.ext import commands
import PIL
from PIL import Image
from PIL import ImageEnhance
import urllib.request
import requests
import random
import os
import re

class Images(commands.Cog):
    def __init__(self, client):
        self.client = client

    @commands.Cog.listener()
    async def on_ready(self):
        print('Images Cog Loaded')
    
    async def open_image(self, path):
        newImage = Image.open(path)
        return newImage

    async def save_image(self, image, path):
        image.save(path, 'png')
    
    async def create_image(self, i, j):
        image = Image.new("RGB", (i, j), "white")
        return image
    async def get_pixel(self, image, i, j):
        # Inside image bounds?
        width, height = image.size
        if i > width or j > height:
          return None
    
        # Get Pixel
        pixel = image.getpixel((i, j))
        return pixel


    async def convert_grayscale(self, image):
        # Get size
        width, height = image.size
        
        # Create new Image and a Pixel Map
        new = await self.create_image(width, height)
        pixels = new.load()
    
        # Transform to grayscale
        for i in range(width):
            for j in range(height):
                # Get Pixel
                pixel = await self.get_pixel(image, i, j)
    
                # Get R, G, B values (This are int from 0 to 255)
                red =   pixel[0]
                green = pixel[1]
                blue =  pixel[2]
    
                # Transform to grayscale
                gray = (red * 0.299) + (green * 0.587) + (blue * 0.114)
    
                # Set Pixel in new image
                pixels[i, j] = (int(gray), int(gray), int(gray))
    
        # Return new image
        return new

    async def convert_pride(self, image):
        x, y = image.size
        size = x, y
        
        if self.client.number == 2:
            im = Image.open("pride2.jpeg")
            im_resized = im.resize(size, Image.ANTIALIAS)
            im_resized.save("pride-res.jpeg")
        elif self.client.number == 3:
            im = Image.open("bi1.jpeg")
            im_resized = im.resize(size, Image.ANTIALIAS)
            im_resized.save("pride-res.jpeg")
        else:
            im = Image.open("pride1.jpeg")
            converter = PIL.ImageEnhance.Color(im)
            im2 = converter.enhance(1.5)
            im_resized = im2.resize(size, Image.ANTIALIAS)
            im_resized.save("pride-res.jpeg")

        im = Image.open("pride-res.jpeg")

        source = im.split()

        R, G, B = 0, 1, 2
        constant = 2.0 # constant by which each pixel is divided
        
        Red = source[R].point(lambda i: i/constant)
        Green = source[G].point(lambda i: i/constant)
        Blue = source[B].point(lambda i: i/constant)
        
        im = Image.merge(im.mode, (Red, Green, Blue))
        im.save("pride-res.jpeg")

        return PIL.ImageChops.add(image, await self.open_image('./pride-res.jpeg'))



    @commands.command()
    async def pride(self, ctx, *args):
        if args == () or args == ('2',) or args == ('3',):
            if "2" in args:
                self.client.number = 2
            elif "3" in args:
                self.client.number = 3
            else:
                self.client.number = 1

            if ctx.message.attachments == []:
                await ctx.send('I can\'t pride nothingness :^)')
            else:
                for item in ctx.message.attachments:
                    req = urllib.request.Request(item.url, headers={'User-Agent' : "Magic Browser"})
                    con = urllib.request.urlopen(req)
                    heximage = con.read()

                    filename = item.url.rsplit('/', 1)[-1]
                    filenameog = filename
                    #filename = f"pride.{}"
                    with open(filename, 'wb') as image:
                        image.write(heximage)

                    self.image = await self.open_image(f'./{filename}')
                    await self.save_image(await self.convert_grayscale(self.image), 'GS')

                    self.image = await self.open_image('GS')
                    filename = f'./prided.{item.url.rsplit(".", 1)[-1]}'
                    await self.save_image(await self.convert_pride(self.image), filename)

                    #print('done!')
                    await ctx.send(file = discord.File(filename))
                    os.remove(filename)
                    os.remove(filenameog)
        else:
            for item in args:
                if item == "2":
                    self.client.number = 2
                    continue
                elif item == "3":
                    self.client.numer = 3
                    continue
                elif args[-1] == "2" or args[0] == "2":
                    self.client.number = 2
                elif args[-1] == "3" or args[0] == "3":
                    self.client.number = 3
                else:
                    self.client.number = 1

                if item == 'last':
                    msg = await ctx.channel.history(limit=3).flatten()
                    oldmsg = msg[1]
                    try:
                        item = oldmsg.content
                        if 'http' not in item:
                            trash = 1/0
                    except:
                        try:
                            item = oldmsg.attachments[0].url
                            if 'http' not in item:
                                trash = 1/0
                        except:
                            await ctx.send('The last message did not contain any image')
                            return


                    req = urllib.request.Request(item, headers={'User-Agent' : "Magic Browser"})
                    con = urllib.request.urlopen(req)
                    heximage = con.read()

                    filename = item.rsplit('/', 1)[-1]
                    filenameog = filename
                    with open(filename, 'wb') as image:
                        image.write(heximage)

                    self.image = await self.open_image(f'./{filename}')
                    await self.save_image(await self.convert_grayscale(self.image), 'pride')

                    self.image = await self.open_image('pride')
                    filename = f'./pride.{item.rsplit(".", 1)[-1]}'
                    await self.save_image(await self.convert_pride(self.image), filename)

                    await ctx.send(file = discord.File(filename))
                    os.remove(filename)
                    os.remove(filenameog)
                else:
                    item = re.sub('[<>]', '', item)
                    req = urllib.request.Request(item, headers={'User-Agent' : "Magic Browser"})
                    con = urllib.request.urlopen(req)
                    heximage = con.read()

                    filename = item.rsplit('/', 1)[-1]
                    filenameog = filename
                    with open(filename, 'wb') as image:
                        image.write(heximage)

                    self.image = await self.open_image(f'./{filename}')
                    await self.save_image(await self.convert_grayscale(self.image), 'pride')

                    self.image = await self.open_image('pride')
                    filename = f'./pride.{item.rsplit(".", 1)[-1]}'
                    await self.save_image(await self.convert_pride(self.image), filename)

                    await ctx.send(file = discord.File(filename))
                    os.remove(filename)
                    os.remove(filenameog)

    @commands.command(aliases = ['gs'])
    async def grayscale(self, ctx, *args):
        if args == ():
            if ctx.message.attachments == []:
                await ctx.send('I can\'t gs nothing :^)')
            else:
                for item in ctx.message.attachments:
                    req = urllib.request.Request(item.url, headers={'User-Agent' : "Magic Browser"})
                    con = urllib.request.urlopen(req)
                    heximage = con.read()

                    filename = item.url.rsplit('/', 1)[-1]
                    filenameog = filename
                    #filename = f"gs.{}"
                    with open(filename, 'wb') as image:
                        image.write(heximage)

                    self.image = await self.open_image(f'./{filename}')
                    filename = f'./gs.{item.url.rsplit(".", 1)[-1]}'
                    await self.save_image(await self.convert_grayscale(self.image), filename)

                    await ctx.send(file = discord.File(filename))
                    os.remove(filename)
                    os.remove(filenameog)
        else:
            for item in args:
                if item == 'last':
                    msg = await ctx.channel.history(limit=3).flatten()
                    oldmsg = msg[1]
                    try:
                        item = oldmsg.content
                        if 'http' not in item:
                            trash = 1/0
                    except:
                        try:
                            item = oldmsg.attachments[0].url
                            if 'http' not in item:
                                trash = 1/0
                        except:
                            await ctx.send('The last message did not contain any image')
                            return

                    req = urllib.request.Request(item, headers={'User-Agent' : "Magic Browser"})
                    con = urllib.request.urlopen(req)
                    heximage = con.read()

                    filename = item.rsplit('/', 1)[-1]
                    filenameog = filename
                    #filename = f"gs.{}"
                    with open(filename, 'wb') as image:
                        image.write(heximage)

                    self.image = await self.open_image(f'./{filename}')
                    filename = f'./gs.{item.rsplit(".", 1)[-1]}'
                    await self.save_image(await self.convert_grayscale(self.image), filename)

                    await ctx.send(file = discord.File(filename))
                    os.remove(filename)
                    os.remove(filenameog)
                else:
                    item = re.sub('[<>]', '', item)
                    req = urllib.request.Request(item, headers={'User-Agent' : "Magic Browser"})
                    con = urllib.request.urlopen(req)
                    heximage = con.read()

                    filename = item.rsplit('/', 1)[-1]
                    filenameog = filename
                    #filename = f"gs.{}"
                    with open(filename, 'wb') as image:
                        image.write(heximage)

                    self.image = await self.open_image(f'./{filename}')
                    filename = f'./gs.{item.rsplit(".", 1)[-1]}'
                    await self.save_image(await self.convert_grayscale(self.image), filename)

                    await ctx.send(file = discord.File(filename))
                    os.remove(filename)
                    os.remove(filenameog)

    @commands.command(aliases=['profile_picture', 'avatar'])
    async def pfp(self, ctx, member: discord.Member = None):
        if member is None:
            member = ctx.author

        embed = discord.Embed()
        embed.set_image(url=member.avatar_url)
        await ctx.send(embed=embed)

def setup(client):
    client.add_cog(Images(client))
