import re
import asyncio
import aiohttp
from bs4 import BeautifulSoup

async def url_call(url):
    async with aiohttp.request("GET", url, headers={'User-Agent' : "Magic Browser"}) as url:
        if url.status == 200:
            return await url.text()

async def prettify_posts(html):
    html_soup = BeautifulSoup(html, "lxml")
    html_span = html_soup.find_all('span')

    gross_thumb_code = []
    thumb_code = []
    img_alt_code = []
    for item in html_span:
        if repr(item).startswith('<span class="thumb">'):
            gross_thumb_code.append(repr(item))

    for item in gross_thumb_code:
        html_soup = BeautifulSoup(item, "lxml")
        html_a = html_soup.find_all('a')

        html_soup = BeautifulSoup(repr(html_a[0]), "lxml")
        html_img = html_soup.find_all('img')

        html_just_a = re.sub(repr(html_img[0]), '', repr(html_a[0]))

        thumb_code.append(html_just_a)
        img_alt_code.append(repr(html_img[0]))

    html_lists = [thumb_code, img_alt_code]
    return html_lists

async def prettify_post(html):
    html_soup = BeautifulSoup(html, "lxml")
    html_div = html_soup.find_all('div')
    html_list = html_soup.find_all('ul')
    # get L46-L50 on post.html instead.

    parameters = []

    for item in html_div:
        if repr(item).startswith('<div id="note-container">'):
            ImgUploaderRaw = repr(item).partition('\n')[0]

    fix = html_list[1].text.split("\n")[1:]
    for i in fix:
        parameters.append(i.split())

    soup = BeautifulSoup(ImgUploaderRaw, "lxml")
    for attr in soup.find_all("div"):
        soup = BeautifulSoup(repr(attr), "lxml")
        for attr in soup.find_all('img'):
            parameters.append(attr['src'])
        for attr in soup.find_all('br'):
            for attr in soup.find_all('a'):
                parameters.append(attr['href'])

    return parameters[:-1]

async def generate_vars(code, base_url):
    initial_dict = {"posts" : []}
    thumb_code = code[0]
    img_alt_code = code[1]

    for num in range(len(thumb_code)):
        f = {}
        soup = BeautifulSoup(img_alt_code[num], "lxml")
        for attr in soup.find_all("img"):
            thumbnail = attr['src']
            tags = attr['title']

        soup = BeautifulSoup(thumb_code[num], "lxml")
        for attr in soup.find_all("a"):
            _id = attr['id'][1:]
            post_url = base_url + attr['href']

        f["id"] = _id
        f["tags"] = tags
        f["post_url"] = post_url
        f["thumbnail"] = thumbnail

        initial_dict["posts"].append(f)

    return initial_dict

async def get_post_info(_dict):
    for post in _dict["posts"]:
        post_url = post["post_url"]
        data = await url_call(post_url)
        x = await prettify_post(data)

        post['date'] = x[1][1] + x[1][2]
        post['uploader'] = x[2][1]
        post['size'] = x[3][1]

        if len(x[4]) != 1:
            post['source'] = x[4][1]
        else:
            post['source'] = None

        post['rating'] = x[5][1]
        post['score'] = x[6][1]
        post['file_url'] = x[7]
        post['sample_url'] = x[7]
        post['author_profile'] = x[8]
        post['md5'] = x[7].split('.')[2].split('/')[-1:]

    return _dict


async def main(base_url, page=0, tags="all"):
    page = 0 if page in (0, 1) else page-1
    pid = 0 if page == 0 else 20*page

    url = base_url + f'index.php?page=post&s=list&tags={tags}&pid={pid}'
    html = await url_call(url)
    posts = await prettify_posts(html)
    initial_dict = await generate_vars(posts, base_url)
    final_dict = await get_post_info(initial_dict)

    return final_dict

#asyncio.run(main('https://footfetishbooru.booru.org/', page=0, tags='tagme'))
