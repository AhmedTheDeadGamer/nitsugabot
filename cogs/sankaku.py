import random
import re
import aiohttp
import aiofiles
import os

import discord
from discord.ext import commands

class SankakuBlack(commands.Cog):
    def __init__(self, client):
        self.client = client

    @commands.Cog.listener()
    async def on_ready(self):
        self.client.threshold = True
        print('SankakuBlack Cog Loaded')

    async def check_nsfw(self, ctx):
        self.client.is_nsfw = ctx.guild is None or ctx.channel.is_nsfw()

    async def send_post(self, ctx, preset_data):
        rating = preset_data[0]
        score = preset_data[1]
        source = preset_data[2]
        full_size = preset_data[3]
        sample = preset_data[4]

        filename = f'{sample.split("/")[-1]}'
        filename = filename.split('?', 1)[0]

        async with aiohttp.request("GET", full_size, headers={'User-Agent' : "Magic Browser"}) as url:
            if url.status == 200:
                f = await aiofiles.open(filename, mode='wb')
                await f.write(await url.read())
                await f.close()


        if rating == 's':
            rating = 'Safe'
        elif rating == 'q':
            rating = 'Questionable'
        elif rating == 'e':
            rating = 'Explicit'

        embed = discord.Embed(colour=discord.Colour(0x222222))

        #embed.set_image(url=sample)
        image = discord.File(filename)
        embed.set_image(url=f'attachment://{filename}')
        print(sample)
        embed.set_author(name="Full size image. (Click here)", url=full_size)
        embed.add_field(name="Rating", value=rating, inline=True)
        embed.add_field(name="Score", value=score, inline=True)

        if source is not None:
            if source != "":
                if 'http' in source:
                    embed.add_field(name="Source", value=f"[Here]({source})", inline=True)
                else:
                    embed.add_field(name="Source", value=source, inline=True)

        print(source)

        self.client.image = await ctx.send(embed=embed, file=image)
        os.remove(filename)
        self.client.source = None


    async def set_data(self, post):
        preset_data = [post['rating']]
        preset_data.append(post['total_score'])
        preset_data.append(post['source'])
        preset_data.append(post['file_url'])
        preset_data.append(post['sample_url'])
        self.client.last_md5 = post['md5']

        return preset_data

    async def page_loop(self, ctx, data, tags):
        if data == []:
            await ctx.send('One of the tags doesn\'t exist.')
            return 727

        page = 0
        while len(data) == 50:
            page += 1
            data = await self.api_call(tags, page)

            if page == 5:
                break
        self.client.datalen = len(data)

        if page < 2:
            pagef = 0
        else:
            pagef = 2
        page += 1
        page = random.choice(range(pagef, page))
        return page

    async def pick_post(self, tags, page):
        data = await self.api_call(tags, page)
        post_num = random.choice(range(0, self.client.datalen))
        post = data[post_num]
        return post

    async def api_call(self, tags, page=0):
        async with aiohttp.request("GET", f"https://capi-v2.sankakucomplex.com/posts?lang=english&tags={tags}&page={page}&limit=100", headers={'User-Agent' : "Magic Browser"}) as url: #opens the url as url
            data = await url.json() #transforms into json
            return data

    async def functions(self, ctx, tags):
        data = await self.api_call(tags)
        page = await self.page_loop(ctx, data, tags)
        if page == 727:
            return
        post = await self.pick_post(tags, page)
        preset_data = await self.set_data(post)
        await self.send_post(ctx, preset_data)

    @commands.command(aliases=['sc', 'sku', 'skku', 'snkku', 'snku', 'pic', 'p', 'picture', '2d'])
    async def sankaku(self, ctx, flag=None, *args):
        await self.check_nsfw(ctx)

        output = ''
        for word in args:
            output += word
            output += '%20'
            if 'rating' in output:
                if self.client.is_nsfw is True:
                    await ctx.send('You are not allowed to use custom ratings outside nsfw channels or dm\'s')
                    return
        output = re.sub("\s", "", (output.lower()))[:-3]

        if self.client.threshold is True:
            output += '%20threshold:1'

        if flag is None:
            output += '%20rating:S'
            await self.functions(ctx, output)
        elif flag == '-r':
            if self.client.is_nsfw is True:
                await self.functions(ctx, output)
            else:
                await ctx.send('You are not allowed to use that flag outside nsfw channels or dm\'s')
        elif flag == '-n':
            if self.client.is_nsfw is True:
                choice = random.choice(['explicit', 'questionable'])
                output += f'%20rating:{choice}'
                await self.functions(ctx, output)
            else:
                await ctx.send('You are not allowed to use that flag outside nsfw channels or dm\'s')
        elif flag == '-x':
            if self.client.is_nsfw is True:
                output += '%20rating:explicit'
                await self.functions(ctx, output)
            else:
                await ctx.send('You are not allowed to use that flag outside nsfw channels or dm\'s')
        elif flag == '-u':
            output = f'rating:S%20user:{output}'
            await self.functions(ctx, output)

            return
        else:
            if 'rating' in flag:
                if self.client.is_nsfw is True:
                    await ctx.send('You are not allowed to use custom ratings outside nsfw channels or dm\'s')
                    return
                else:
                    output += f'%20{flag}'
            else:
                if 'rating' in output:
                    output += f'%20{flag}'
                else:
                    output += f'%20rating:S%20{flag}'
            await self.functions(ctx, output)
    @commands.command(aliases=['threshold', 'set_rating', 'change_force_rating', 'disable_threshold'])
    async def set_threshold(self, ctx):
        self.client.threshold = not self.client.threshold
        await ctx.send(f'Set tot {self.client.threshold}')


def setup(client):
    client.add_cog(SankakuBlack(client))
