import json
import re
import random
from termcolor import colored
import requests
import discord
from discord.ext import commands


class yandere(commands.Cog):
    def __init__(self, client):
        self.client = client

    @commands.Cog.listener()
    async def on_ready(self):
        print('Yandere Cog Loaded')
        print(colored('-------', 'magenta'))

    async def get_artist(self, yandere):
        for tag in yandere[0]['tags'].split():
            print(tag)
            url_artist = f'https://yande.re/artist.json?name={tag}&commit=Search&order=name'
            yandere_artist_json = requests.get(url_artist)
            yandere_artist = json.loads(yandere_artist_json.content)

            if yandere_artist == []:
                pass
            else:
                if yandere_artist[0]['name'] == tag:
                    return tag

        #for artist in 
    async def send_post(self, ctx, sample, rating, score, source, full_size):
        if rating == 's':
            rating = 'Safe'
        elif rating == 'q':
            rating = 'Questionable'
        elif rating == 'e':
            rating = 'Explicit'

        embed = discord.Embed(colour=discord.Colour(0x222222))

        embed.set_image(url=sample)
        embed.set_author(name="Full size image. (Click here)", url=full_size)

        embed.add_field(name="Rating", value=rating, inline=True)
        embed.add_field(name="Score", value=score, inline=True)
        if source != '':
            if 'http' in source:
                embed.add_field(name="Source", value=f"[Here]({source})", inline=True)
            else:
                embed.add_field(name="Source", value=source, inline=True)

        self.client.image = await ctx.send(embed=embed)
        self.client.source = None


    async def api_call(self, ctx, tags):
        base_url = 'https://yande.re/post.json?tags='
        page = 0
        url = f'https://yande.re/post.json?tags={tags}&page={str(page)}'
        yandere_json = requests.get(url)
        yandere = json.loads(yandere_json.content)

        if len(yandere) == 0:
            await ctx.send('One of the tags doesn\'t exist.')
            return

        while len(yandere) == 40:
            page += 1
            url = f'https://yande.re/post.json?tags={tags}&page={str(page)}'
            yandere_json = requests.get(url)
            yandere = json.loads(yandere_json.content)

            if page == 10:
                break

        page += 1
        page = random.choice(range(0, page))

        url = f'https://yande.re/post.json?tags={tags}&page={str(page)}'
        yandere_json = requests.get(url)
        yandere = json.loads(yandere_json.content)

        pick = yandere[random.choice([0, (len(yandere) - 1)])]

        rating = pick['rating']
        score = pick['score']
        source = pick['source']
        full_size = pick['file_url']
        sample = pick['sample_url']

        self.client.last_md5 = pick['md5']

        await self.send_post(ctx, sample, rating, score, source, full_size)

    @commands.command(aliases = ['ydr', 'yd'])
    async def yandere(self, ctx, flag=None, *args):
        output = ''
        for word in args:
            output += word
            output += '%20'
            if 'rating' in output:
                if ctx.guild != None and ctx.channel.is_nsfw() == False:
                    await ctx.send('You are not allowed to use custom ratings outside nsfw channels or dm\'s')
                    return
        output = re.sub("\s", "", (output.lower()))[:-3]

        if flag == None:
            output += '%20rating:S'
            await self.api_call(ctx, output)
        elif flag == '-r':
            if ctx.guild == None or ctx.channel.is_nsfw():
                await self.api_call(ctx, output)
            else:
                await ctx.send('You are not allowed to use that flag outside nsfw channels or dm\'s')
        elif flag == '-n':
            if ctx.guild == None or ctx.channel.is_nsfw():
                choice = random.choice(['E', 'Q'])
                output += f'%20rating:{choice}'
                await self.api_call(ctx, output)
            else:
                await ctx.send('You are not allowed to use that flag outside nsfw channels or dm\'s')
        elif flag == '-x':
            if ctx.guild == None or ctx.channel.is_nsfw():
                output += '%20rating:E'
                await self.api_call(ctx, output)
            else:
                await ctx.send('You are not allowed to use that flag outside nsfw channels or dm\'s')
        elif flag == '-u':
            output = f'rating:S%20user:{output}'
            await self.api_call(ctx, output)

            return
        else:
            if 'rating' in flag:
                if ctx.guild != None and ctx.channel.is_nsfw() == False:
                    await ctx.send('You are not allowed to use custom ratings outside nsfw channels or dm\'s')
                    return
                else:
                    output += f'%20{flag}'
            else:
                if 'rating' in output:
                    output += f'%20{flag}'
                else:
                    output += f'%20rating:S%20{flag}'
            await self.api_call(ctx, output)


    @commands.command()
    async def tags(self, ctx, parameter=None):
        url = f'https://yande.re/post.json?tags=md5:{self.client.last_md5}'
        yandere_json = requests.get(url)
        yandere = json.loads(yandere_json.content)


        if parameter == 'creator':
            parameter = await self.get_artist(yandere)
            await ctx.send(parameter)
        elif parameter == 'artist':
            parameter = await self.get_artist(yandere)
            await ctx.send(parameter)
        elif parameter == 'author':
            parameter = await self.get_artist(yandere)
            await ctx.send(parameter)
        else:
            if parameter == None:
                tags = yandere[0]['tags']
                await ctx.send(f'```{tags}```')
            else:
                try:
                    if parameter == 'uploader':
                        parameter = 'author'
                    thing = yandere[0][parameter]
                    await ctx.send(str(thing))
                except:
                    await ctx.send('This is the list of parameters you can use: <http://5124.mywire.org/HDD/SS/21:32:16_29-07-2019.png>')




def setup(client):
    client.add_cog(yandere(client))

