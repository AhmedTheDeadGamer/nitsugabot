""" this file is the reddit cog
"""
import random

import aiohttp
import discord
from discord.ext import commands

class Reddit(commands.Cog):
    def __init__(self, client):
        self.client = client

    @commands.Cog.listener()
    async def on_ready(self):
        print('Reddit Cog Loaded')

    async def picker(self, data):
        try:
            rpost = random.randint(1, len(data['data']['children'])) #picks a random post
        except ValueError:
            return 727

        for _ in range(10): #loop to retry max 10
            while True:
                if self.client.is_nsfw is False:
                    try:
                        if data['data']['children'][rpost]['data']['preview']['images'][0]['variants'] != {}: #checks if the post is sfw
                            rpost += 1
                            continue
                    except IndexError:
                        return 727
                break #breaks loop when it picks a post that's ok for the channel

        return rpost

    async def sender(self, ctx, data, rpost):
        if rpost == 727:
            if self.client.is_nsfw is True:
                await ctx.send('Subreddit/User doesn\'t exist.')
            else:
                await ctx.send('Subreddit/User doesn\'t exist or only contains NSFW posts.')
            return
        embed = discord.Embed(title="OG Post", colour=discord.Colour(0xff78e6), url=f"https://reddit.com{data['data']['children'][rpost]['data']['permalink']}") #embeds source
        embed.set_image(url=f"{data['data']['children'][rpost]['data']['url']}") #embreds image

        self.client.reddit = await ctx.send(embed=embed) #sends embed

    async def sub_api_call(self, subreddit): #picks a post and sends it as an embed
        async with aiohttp.request("GET", f"https://www.reddit.com/r/{subreddit}/top.json?limit=100&t=month", headers={'User-Agent' : "Magic Browser"}) as url:
            data = await url.json() #transforms into json
            return data

    async def user_api_call(self, username):
        async with aiohttp.request("GET", f"https://www.reddit.com/user/{username}/top.json?limit=100&t=month", headers={'User-Agent' : "Magic Browser"}) as url:
            data = await url.json() #transforms into json
            return data

    async def check_nsfw(self, ctx): #nsfw channel check
        self.client.is_nsfw = ctx.guild is None or ctx.channel.is_nsfw() #checks if the command was invoked on an nsfw channel or on a dm

    @commands.command(aliases=['traps'])
    async def trap(self, ctx):
        await self.check_nsfw(ctx)
        data = await self.sub_api_call('CuteTraps')
        rpost = await self.picker(data)
        await self.sender(ctx, data, rpost)

    @commands.command(aliases=['ruser', 'userpost', 'upost', 'rup', 'ru'])
    async def reddit_user_post(self, ctx, username=None):
        if username is None:
            await ctx.send('Please, specify a user')
            return
        await self.check_nsfw(ctx)
        data = await self.user_api_call(username)
        rpost = await self.picker(data)
        await self.sender(ctx, data, rpost)

    @commands.command(aliases=['rpost', 'reddit', 'r', 'rp', 'redditpost'])
    async def reddit_post(self, ctx, subreddit=None):
        if subreddit is None:
            await ctx.send('Please, specify a subreddit')
            return
        await self.check_nsfw(ctx)
        data = await self.sub_api_call(subreddit)
        rpost = await self.picker(data)
        await self.sender(ctx, data, rpost)

    @commands.command(aliases=['ruserbomb', 'userbomb', 'ubomb', 'rub'])
    async def reddit_user_bomb(self, ctx, username=None, amount=None):
        if amount is None:
            amount = 5
        else:
            amount = int(amount)
            if amount >= 6:
                amount = 5

        if username is None:
            await ctx.send('Please, specify a user')
            return

        await self.check_nsfw(ctx)
        data = await self.user_api_call(username)
        links = ''
        for _ in range(amount):
            rpost = await self.picker(data)

            if rpost == 727:
                if self.client.is_nsfw is True:
                    await ctx.send('User doesn\'t exist.')
                else:
                    await ctx.send('User doesn\'t exist or only contains NSFW posts.')
                return

            links += f" {data['data']['children'][rpost]['data']['url']}"

        await ctx.send(links)

    @commands.command(aliases=['rbomb', 'redditbomb'])
    async def reddit_bomb(self, ctx, subreddit=None, amount=None):
        if amount is None:
            amount = 5
        else:
            amount = int(amount)
            if amount >= 6:
                amount = 5

        if subreddit is None:
            await ctx.send('Please, specify a user')
            return

        await self.check_nsfw(ctx)
        data = await self.sub_api_call(subreddit)
        links = ''
        for _ in range(amount):
            rpost = await self.picker(data)

            if rpost == 727:
                if self.client.is_nsfw is True:
                    await ctx.send('Subreddit doesn\'t exist.')
                else:
                    await ctx.send('Subreddit doesn\'t exist or only contains NSFW posts.')
                return

            links += f" {data['data']['children'][rpost]['data']['url']}"

        await ctx.send(links)

    @commands.command(aliases=['hb', 'hentaibomb'])
    @commands.is_nsfw()
    async def hentai_bomb(self, ctx, amount=None):
        await ctx.invoke(self.client.get_command('reddit_user_bomb'), 'afterdarkstash', amount)

    @commands.command(aliases=['remove', 'del', 'dd'])
    async def delete(self, ctx, post=None):
        if post in ['reddit', 'r', 'red']:
            await self.client.reddit.delete(content="oof")
            try:
                await self.client.reddit.edit(content="oof")
            except:
                pass
        elif post in ['picture', 'pic', 'bestgirl', 'p', 'bg']:
            await self.client.image.delete()
            try:
                await self.client.source.edit(content=f"{self.client.source.content}\nOriginal image was deleted, open the links at your own risk.")
            except:
                pass
        elif post is None:
            await ctx.send('use: food!delete <reddit/picture>')
        else:
            await ctx.send(f'{post} is not something you can delete.')

def setup(client):
    client.add_cog(Reddit(client))
