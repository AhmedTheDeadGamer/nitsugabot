import json
import discord
from discord.ext import commands

class Tags(commands.Cog):
    def __init__(self, client):
        self.client = client

    @commands.Cog.listener()
    async def on_ready(self):
        print('Tags Cog Loaded')

    async def read_json(self):
        with open('tags.json', 'r') as tagson:
            data = json.load(tagson)
        return data

    async def write_json(self, data):
        with open('tags.json', 'w') as tagson:
            json.dump(data, tagson, indent=4, sort_keys=True)
        with open('tags.json', 'r') as tagson:
            new_data = json.load(tagson)
        return new_data

    async def get_author_guild(self, ctx):
        author_id = str(ctx.author.id)
        try:
            guild_id = str(ctx.guild.id)
        except AttributeError:
            guild_id = None
        return (author_id, guild_id)


    async def find_tag_data(self, tag_name, invoker_id, guild_id):
        data = await self.read_json()

        for user, tags in data.items():
            if user == invoker_id:
                for tag, data in tags.items():
                    if tag == tag_name:
                        return [data, user]
            elif guild_id is None:
                return [0, 0]
            else:
                for tag, data in tags.items():
                    if tag == tag_name:
                        if data['guild'] == guild_id:
                            return [data, user]
        return [1, 1]
    async def find_owner_only_tag(self, tag_name, invoker_id, guild_id):
        data = await self.read_json()

        for user, tags in data.items():
            if user == invoker_id:
                for tag, data in tags.items():
                    if tag == tag_name:
                        return data
        return 0

    async def before_create_check(self, tag_name, guild_id):
        if guild_id is None:
            return False
        data = await self.read_json()
        for user, tags in data.items():
            for tag, data in tags.items():
                if tag == tag_name:
                    if data['guild'] == guild_id:
                        return True
        return False



    @commands.command()
    async def tag(self, ctx, mode=None, tag=None, *, message=None):
        if mode is None:
            return await ctx.send("Please specify a tag or action.")
        reserved_words = ("edit", "modify", "add", "new", "create", "delete", "remove", "help", "?", "list", "all", "alias", "info", "search", "find", "stats", "transfer", "give", "owner")
        if mode in ('edit', 'modify'):
            if message is None:
                return await ctx.send('Please specify a message for the tag, or use `delete` rather than `edit` if you\'d like to remove it.')

            author_id, guild_id = await self.get_author_guild(ctx)

            data = await self.find_tag_data(tag, author_id, guild_id)
            user = data[1]
            data = data[0]

            if data == 0:
                return await ctx.send(f'Tag "{tag}" doesn\'t exist.')
            elif data == 1:
                return await ctx.send(f'Tag "{tag}" doesn\'t exist or is not owned by you.')
            elif user != author_id:
                return await ctx.send(f'Tag "{tag}" is not owned by you.')

            full_data = await self.read_json()
            full_data[author_id][tag]['message'] = message

            await self.write_json(full_data)

            return await ctx.send(f'Successfully edited tag "{tag}"')

        elif mode in ('add', 'new', 'create'):
            if tag is None:
                return await ctx.send('Please specify a tag name and a message.')
            elif tag in reserved_words:
                return await ctx.send("This tag name is a reserved name, do not use it.")
            elif message is None:
                return await ctx.send('Please specify a message for the tag.')

            author_id, guild_id = await self.get_author_guild(ctx)

            can_i = await self.before_create_check(tag, guild_id)
            if can_i: return await ctx.send(f'Tag {tag} already exists in this guild.')

            full_data = await self.read_json()
            new_dict = {'message' : message, 'guild' : guild_id}
            try:
                full_data[author_id][tag] = new_dict
            except KeyError:
                full_data[author_id] = {}
                full_data[author_id][tag] = new_dict


            await self.write_json(full_data)

            return await ctx.send(f'Successfully created tag "{tag}"')
        elif mode in ('delete', 'remove'):
            if tag is None:
                return await ctx.send('Please specify a tag name and a message.')

            author_id, guild_id = await self.get_author_guild(ctx)
            owner = await self.find_owner_only_tag(tag, author_id, guild_id)
            if owner == 0:
                return await ctx.send(f'Tag {tag} doesn\'t exist or is not owned by you')

            full_data = await self.read_json()
            new_dict = full_data[author_id].pop(tag)

            await self.write_json(full_data)

            return await ctx.send(f'Successfully removed tag "{tag}"')
        elif mode in ('?', 'help'):
            await ctx.invoke(self.client.get_command('help'), 'tags')
        elif mode in ('list', 'all'):
            await ctx.send('WIP')
        else:
            if tag is None:
                tag_name = mode
            elif message is None:
                tag_name = f"{mode} {tag}"
            else:
                tag_name = f"{mode} {tag} {message}"

            author_id, guild_id = await self.get_author_guild(ctx)

            data = await self.find_tag_data(tag_name, author_id, guild_id)
            data = data[0]
            if data != 0 and data != 1:
                message = data['message']
                message = await commands.clean_content().convert(ctx, message)
                await ctx.send(message)
            else: await ctx.send(f'Tag "{tag_name}" not found')

def setup(client):
    client.add_cog(Tags(client))
